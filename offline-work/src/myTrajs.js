const traj1 = {
  uid: 9990,
  pid: 9990,
  building: 0,
  floor: 0,
  points: [
    {
      coordinate: {latitude: 39.993046, longitude: -0.069045},
      location: {id: '016', inside: false},
      time: 1494237600
    },
    {
      coordinate: {latitude: 39.993048, longitude: -0.069013},
      location: {id: '015', inside: false},
      time: 1494237605
    },
    {
      coordinate: {latitude: 39.993053, longitude: -0.068993},
      location: {id: '014', inside: false},
      time: 1494237609
    },
    {
      coordinate: {latitude: 39.993074, longitude: -0.068765},
      location: {id: '008', inside: false},
      time: 1494496800
    },
    {
      coordinate: {latitude: 39.993059, longitude: -0.068734},
      location: {id: '036', inside: false},
      time: 1494496805
    },
  ]
};

const traj2 = {
  uid: 9991,
  pid: 9991,
  building: 0,
  floor: 0,
  points: [
    {
      coordinate: {latitude: 39.993046, longitude: -0.069045},
      location: {id: '016', inside: false},
      time: 1494237600
    },
    {
      coordinate: {latitude: 39.993048, longitude: -0.069013},
      location: {id: '015', inside: false},
      time: 1494237605
    },
    {
      coordinate: {latitude: 39.993053, longitude: -0.068993},
      location: {id: '014', inside: false},
      time: 1494237609
    },
    {
      coordinate: {latitude: 39.993074, longitude: -0.068765},
      location: {id: '008', inside: false},
      time: 1494237610
    },
    {
      coordinate: {latitude: 39.993037, longitude: -0.068977},
      location: {id: '019', inside: true},
      time: 1494237612
    },
  ]
};

const traj3 = {
  uid: 9992,
  pid: 9992,
  building: 0,
  floor: 0,
  points: [
    {
      coordinate: {latitude: 39.993046, longitude: -0.069045},
      location: {id: 'TI2-016', inside: false},
      time: 1494237600
    },
    {
      coordinate: {latitude: 39.993048, longitude: -0.069013},
      location: {id: 'TI2-015', inside: false},
      time: 1494237605
    },
    {
      coordinate: {latitude: 39.993053, longitude: -0.068993},
      location: {id: 'TI2-014', inside: false},
      time: 1494237609
    },
    {
      coordinate: {latitude: 39.993053, longitude: -0.068993},
      location: {id: 'TI2-013', inside: true},
      time: 1494237613
    },
    {
      coordinate: {latitude: 39.993053, longitude: -0.068993},
      location: {id: 'TI2-013', inside: true},
      time: 1494237614
    },
    {
      coordinate: {latitude: 39.993053, longitude: -0.068993},
      location: {id: 'TI2-013', inside: true},
      time: 1494237614
    },
    {
      coordinate: {latitude: 39.993053, longitude: -0.068993},
      location: {id: 'TI2-013', inside: true},
      time: 1494237615
    },
    {
      coordinate: {latitude: 39.993053, longitude: -0.068993},
      location: {id: 'TI2-013', inside: true},
      time: 1494237615
    },
    {
      coordinate: {latitude: 39.993053, longitude: -0.068993},
      location: {id: 'TI2-013', inside: true},
      time: 1494237616
    },
    {
      coordinate: {latitude: 39.993053, longitude: -0.068993},
      location: {id: 'TI2-013', inside: true},
      time: 1494237616
    },
    {
      coordinate: {latitude: 39.993053, longitude: -0.068993},
      location: {id: 'TI2-013', inside: true},
      time: 1494237617
    },
    {
      coordinate: {latitude: 39.993053, longitude: -0.068993},
      location: {id: 'TI2-013', inside: true},
      time: 1494237617
    },
    {
      coordinate: {latitude: 39.993053, longitude: -0.068993},
      location: {id: 'TI2-013', inside: true},
      time: 1494237617
    },
    {
      coordinate: {latitude: 39.993053, longitude: -0.068993},
      location: {id: 'TI2-013', inside: true},
      time: 1494237780
    },
    {
      coordinate: {latitude:  39.993053, longitude: -0.068993},
      location: {id: 'TI2-021', inside: false},
      time: 1494237783
    },
    {
      coordinate: {latitude:  39.993053, longitude: -0.068993},
      location: {id: 'TI2-021', inside: false},
      time: 1494237784
    },
    {
      coordinate: {latitude:  39.993053, longitude: -0.068993},
      location: {id: 'TI2-021', inside: false},
      time: 1494237785
    },
  ]
};

const traj4 = {
  uid: 9993,
  pid: 9993,
  building: 0,
  floor: 0,
  points: [
    {
      coordinate: '39.993236, -0.068568',
      location: {id: 'TI1-002', inside: false},
      time: 1494336600
    },
    {
      coordinate: '39.993280, -0.068590',
      location: {id: 'TI1-007', inside: false},
      time: 1494336603
    },
    {
      coordinate: '39.993317, -0.068566',
      location: {id: 'TI1-008', inside: false},
      time: 1494336605
    },
    {
      coordinate: '39.993364, -0.068569',
      location: {id: 'TI1-022', inside: true},
      time: 1494336608
    },
    {
      coordinate: '39.993364, -0.068569',
      location: {id: 'TI1-022', inside: true},
      time: 1494336609
    },
    {
      coordinate: '39.993364, -0.068569',
      location: {id: 'TI1-022', inside: true},
      time: 1494336900
    },
    {
      coordinate: '39.993364, -0.068569',
      location: {id: 'TI1-022', inside: true},
      time: 1494336901
    },
    {
      coordinate: '39.993425, -0.068590',
      location: {id: 'TI1-023', inside: false},
      time: 1494336905
    },
    {
      coordinate: '39.993456, -0.068598',
      location: {id: 'TI1-025', inside: false},
      time: 1494336907
    },
    {
      coordinate: '39.993364, -0.068569',
      location: {id: 'TI1-022', inside: true},
      time: 1494336911
    },
    {
      coordinate: '39.993364, -0.068569',
      location: {id: 'TI1-022', inside: true},
      time: 1494337050
    },
    {
      coordinate: '39.993364, -0.068569',
      location: {id: 'TI1-022', inside: true},
      time: 1494337052
    },
  ]
};

const traj5 = {
  uid: 9994,
  pid: 9994,
  building: 0,
  floor: 0,
  points: [
    {
      coordinate: '39.993445, -0.068596',
      location: {id: 'TI1-025', inside: false},
      time: 1494424800
    },
    {
      coordinate: '39.993427, -0.068588',
      location: {id: 'TI1-023', inside: false},
      time: 1494424804
    },
    {
      coordinate: '39.993366, -0.068566',
      location: {id: 'TI1-022', inside: true},
      time: 1494424807
    },
    {
      coordinate: '39.993366, -0.068566',
      location: {id: 'TI1-022', inside: true},
      time: 1494424808
    },
    {
      coordinate: '39.993366, -0.068566',
      location: {id: 'TI1-022', inside: true},
      time: 1494424940
    },
    {
      coordinate: '39.993366, -0.068566',
      location: {id: 'TI1-022', inside: true},
      time: 1494424941
    },
    {
      coordinate: '39.993364, -0.068486',
      location: {id: 'TI1-011', inside: false},
      time: 1494424946
    },
    {
      coordinate: '39.993353, -0.068401',
      location: {id: 'TI1-013', inside: false},
      time: 1494424950
    },
    {
      coordinate: '39.993362, -0.068361',
      location: {id: 'TI1-014', inside: false},
      time: 1494424953
    },
    {
      coordinate: '39.993362, -0.068361',
      location: {id: 'TI1-015', inside: true},
      time: 1494424954
    },
    {
      coordinate: '39.993362, -0.068361',
      location: {id: 'TI1-015', inside: true},
      time: 1494424955
    },
    {
      coordinate: '39.993362, -0.068361',
      location: {id: 'TI1-015', inside: true},
      time: 1494425076
    },
    {
      coordinate: '39.993362, -0.068361',
      location: {id: 'TI1-015', inside: true},
      time: 1494425078
    },
    {
      coordinate: '39.993385, -0.068246',
      location: {id: 'TI1-016', inside: false},
      time: 1494425083
    },
    {
      coordinate: '39.993391, -0.068225',
      location: {id: 'TI1-016', inside: false},
      time: 1494425083
    },
    {
      coordinate: '39.993362, -0.068361',
      location: {id: 'TI1-015', inside: true},
      time: 1494425089
    },
    {
      coordinate: '39.993362, -0.068361',
      location: {id: 'TI1-015', inside: true},
      time: 1494425240
    },
    {
      coordinate: '39.993362, -0.068361',
      location: {id: 'TI1-015', inside: true},
      time: 1494425243
    },
  ]
};

const traj6 = {
  uid: 9995,
  pid: 9995,
  building: 0,
  floor: 0,
  points: [
    {
      coordinate: '39.993445, -0.068596',
      location: {id: 'TI1-025', inside: false},
      time: 1494424800
    },
    {
      coordinate: '39.993427, -0.068588',
      location: {id: 'TI1-023', inside: false},
      time: 1494424804
    },
    {
      coordinate: '39.993366, -0.068566',
      location: {id: 'TI1-022', inside: true},
      time: 1494424807
    },
    {
      coordinate: '39.993366, -0.068566',
      location: {id: 'TI1-022', inside: true},
      time: 1494424808
    },
    {
      coordinate: '39.993366, -0.068566',
      location: {id: 'TI1-022', inside: true},
      time: 1494424940
    },
    {
      coordinate: '39.993366, -0.068566',
      location: {id: 'TI1-022', inside: true},
      time: 1494424941
    },
    {
      coordinate: '39.993364, -0.068486',
      location: {id: 'TI1-011', inside: false},
      time: 1494424946
    },
    {
      coordinate: '39.993353, -0.068401',
      location: {id: 'TI1-013', inside: false},
      time: 1494424950
    },
    {
      coordinate: '39.993362, -0.068361',
      location: {id: 'TI1-014', inside: false},
      time: 1494424953
    },
    {
      coordinate: '39.993362, -0.068361',
      location: {id: 'TI1-015', inside: true},
      time: 1494424954
    },
    {
      coordinate: '39.993362, -0.068361',
      location: {id: 'TI1-015', inside: true},
      time: 1494424955
    },
    {
      coordinate: '39.993362, -0.068361',
      location: {id: 'TI1-015', inside: true},
      time: 1494425076
    },
    {
      coordinate: '39.993362, -0.068361',
      location: {id: 'TI1-015', inside: true},
      time: 1494425078
    },
    {
      coordinate: '39.993385, -0.068246',
      location: {id: 'TI1-016', inside: false},
      time: 1494425083
    },
    {
      coordinate: '39.993391, -0.068225',
      location: {id: 'TI1-016', inside: false},
      time: 1494425083
    },
  ]
};

const traj7 = {
  uid: 9996,
  pid: 9996,
  building: 0,
  floor: 0,
  points: [
    {
      coordinate: '39.993583, -0.068649',
      location: {id: 'TI1-030', inside: false},
      time: 1495105200
    },
    {
      coordinate: '39.993525, -0.068624',
      location: {id: 'TI1-028', inside: false},
      time: 1495105210
    },
    {
      coordinate: '39.993457, -0.068598',
      location: {id: 'TI1-025', inside: false},
      time: 1495105213
    },
    {
      coordinate: '39.993412, -0.068582',
      location: {id: 'TI1-023', inside: false},
      time: 1495105216
    },
    {
      coordinate: '39.993382, -0.068521',
      location: {id: 'TI1-009', inside: false},
      time: 1495105221
    },
    {
      coordinate: '39.993351, -0.068456',
      location: {id: 'TI1-011', inside: false},
      time: 1495105228
    },
    {
      coordinate: '39.993354, -0.068405',
      location: {id: 'TI1-013', inside: false},
      time: 1495105233
    },
    {
      coordinate: '39.993366, -0.068339',
      location: {id: 'TI1-014', inside: false},
      time: 1495105237
    },
    {
      coordinate: '39.993381, -0.068280',
      location: {id: 'TI1-015', inside: false},
      time: 1495105241
    },
  ],
};

export default [
  traj1,  // trip segmentation
  traj2,  // noise filtering
  traj3,  // spd. and segmentation
  traj4,  // hot-spots & hot-routes
  traj5,  // hot-spots & hot-routes
  traj6,  // hot-spots & hot-routes
  traj7,  // compression
];
