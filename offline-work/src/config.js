// Trip
const trip = {
  len: 3,  // 20 points in trips
  duration: 1440  // a day
};

// Noise Filtering
const nf = {
  speed: 3,  // m/s
};

// Stay Point Detection
const spd = {
  stayTime: 20,  // second
};

// Segmentation
const seg = {
  timeIntervalBased: {
    timeThreshold: 30,
  },
  stayPointBased: {},
};

// Compression
const compression = {
  complexity: 70,
  highQuality: true,
  tolerance: 1.2,
};

export default {
  trip,
  nf,
  spd,
  seg,
  compression,
};
