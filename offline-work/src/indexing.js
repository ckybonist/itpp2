import fs from 'fs'
import path from 'path'
import Baby from 'babyparse'
import _ from 'lodash'
import mongoose from 'mongoose'

import {
  Trajectory,
  MetaData,
} from './trajectory.model'


mongoose.Promise = Promise;  // mongoose's mPromise is deprecated


function initDB() {
  const options = { promiseLibrary: require('bluebird') };
  mongoose.createConnection('mongodb://localhost/Analysis', options);
  const db = mongoose.connection;
  db.on('error', console.error.bind(console, 'connection error...'));
}


function toDate(unixtime) {
  return new Date(unixtime * 1000)  // second to millisecond
  // console.log(time)
  // console.log(`
  //   Date: ${time.getFullYear()}-${time.getMonth()}-${time.getDay()}
  //   Time: ${time.getHours()}:${time.getMinutes()}:${time.getSeconds()}
  // `)
}

function readFile(fname) {
  return new Promise((resolve, reject) => {
    fs.readFile(fname, {encoding: 'utf8'},
      (err, content) => {
        if (err)
          reject(err)
        else
          resolve(content)
    })
  })
}

function formatTrajData(row) {
  return {
    coordinate: {
      latitude: parseFloat(row[521]),
      longitude: parseFloat(row[520]),
    },
    location: {
      id: row[524],
      inside: parseInt(row[525]) === 1
    },
    time: toDate(row[528]),
    rssis: row.slice(0, 520).map(parseFloat),
  };
}

function formatTrajId(row) {
  return {
    uid: parseInt(row[526]),
    building: parseInt(row[523]),
    floor: parseInt(row[522]),
    pid: parseInt(row[527]),
  }
}

function noNaN(row) {
  let flag = true;

  for (const k of Object.keys(row)) {
    if ( _.isNaN(row[k]) ) {
      flag = false;
    }
  }

  return flag;
}

function findTrajIds(data) {
  const ids = data.map(formatTrajId);
  return _.uniqWith(ids, _.isEqual);
}

function findUserIds(trajIDs) {
  const pick = (tid) => _.pick(tid, ['uid']);

  return _.uniqWith(trajIDs.map(pick), _.isEqual)
          .map(e => e.uid);
}


function constructMetaData(data) {
  const trajIds = findTrajIds(data).filter(noNaN);
  const numBuildings = 3;


  const findNumFloors = (bid) => {
    const floors = trajIds
                    .filter(id => id.building === bid)
                    .map(id => id.floor);

    return _.uniq(floors).length;
  };
  const numFloors = _.range(numBuildings)
                     .map(findNumFloors);

  return {
    trajIds,
    userIds: findUserIds(trajIds),
    numBuildings,
    numFloors,
    numPhones: 25,
  };
}

function constructTrajs(data, trajIds) {
  const match = (row, id) =>
    _.isEqual(formatTrajId(row), id);

  const trajectories = [];

  for (const id of trajIds) {
    const points = data.filter(row => match(row, id))
                       .map(formatTrajData);
     const traj = Object.assign({}, id, { points });
     trajectories.push(traj);
  }

  return trajectories;
}

function saveDB(metaData, trajectories) {
  const handler = (err, docs) => {
    if (err) {
      console.error(err);
    } else {
      Trajectory.collection.insert(trajectories);
      mongoose.disconnect();
      console.log('Finishing db works!');
    }
  };

  MetaData.collection.insert(metaData, handler)
}

function indexing(csvContent) {
  initDB();

  const csvParser = Baby.parse;
  const data = csvParser(csvContent).data.slice(1);

  const metaData = constructMetaData(data);
  const trajectories = constructTrajs(data, metaData.trajIds);

  saveDB(metaData, trajectories);
}

export default function() {
  const fname = path.resolve(__dirname, '../../raw-data/trainingData.csv');

  readFile(fname)
    .then(indexing)
    .catch(err => console.error(err));
}
