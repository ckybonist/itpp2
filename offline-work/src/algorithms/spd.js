import _ from 'lodash';
import { ArraySlice, range, ms2sec } from '../utils/utils'


class StayPoint {
  constructor(location, arrivalTime, leaveTime, startIndex, endIndex) {
    this.location = location;
    this.arrivalTime = arrivalTime;
    this.leaveTime = leaveTime;
    this.startIndex = startIndex;
    this.endIndex = endIndex;
  }
}

function findStayRegions(trajectory, THRES_TIME) {
  const NUM_POINTS = trajectory.length;
  const regions = [];

  let i = 0;
  let j = 0;
  while (i < NUM_POINTS) {
    /* IMPORTANT:
     * Prevent infinite loop that happend when:
     *  j === NUM_POINTS - 1
     */

    if (j >= NUM_POINTS-1) {
      break;
    } else {
      j = i + 1;
    }

    const region = [];
    const pi = trajectory[i];
    const nextLocationId = trajectory[j].location.id;
    const duration = ms2sec(trajectory[j].time - pi.time);


    // Point will add to region in two cases:
    // 1. Repeated points
    // 2. If the next point is different, it also check the duration
    // between two points are larger enough. i.e. stay region with signle point.
    //
    // NOTE: case 2 is kind of rare, this possibly happend when user
    // turn off WiFi for a long period in the region.
    if (pi.location.id == nextLocationId || duration >= THRES_TIME) {
      region.push({data: pi, index: i})
    }

    while (j < NUM_POINTS) {
      const pj = trajectory[j];

      if (pj.location.id !== pi.location.id) {
        i = j;
        break;
      }

      region.push({ data: pj, index: j });
      j += 1;
    }

    regions.push(region);
  } // END WHILE

  return regions.filter(r => r.length > 0);
}

export default function detectStayPoints(trajectory, THRES_TIME) {
  const regions = findStayRegions(trajectory, THRES_TIME);

  const stayTimes = [];  // for debug


  const stayPoints = regions.filter(reg => {
      const p1 = reg[0].data.time;
      const p2 = _.last(reg).data.time;
      const duration = ms2sec(p2 - p1);
      stayTimes.push(duration);

      return duration >= THRES_TIME;
    })
    .map(region => {
      const first= region[0];
      const last= _.last(region);
      return new StayPoint(
        first.data.location,
        first.data.time,
        last.data.time,
        first.index,
        last.index
      );
    });

  return { stayPoints, stayTimes };
}
