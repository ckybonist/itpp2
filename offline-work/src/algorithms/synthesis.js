/*
  # Goal:
    - contains top 3 popular routes
    - some revisited points
    - suppose each user only visit once

  # Base structure:
    - Stay trajectory: [ stayPoint1, stayPoint2, ... ];
    - Synthetic stay point:
      { roomId, duration, arrivalTime, leaveTime };
*/

const _ = require('lodash');


const THRES_TRIP = 180;  // minutes
const STAY_TRAJ_LENGTH = 50;
const MIN_STAY_TIME = 3;
const MAX_STAY_TIME = 60;
const MIN_HOURS = 10;
const MAX_HOURS = 22;

function rangeRand(min, max) {
  return Math.random() * (max - min) | min;
}

function randHours(start, end) {
  return 0 | start + Math.random() * (end-start);
}

function createStayTimes() {
  let timestamps = [];
  let totalDuration = 0;
  const h = rangeRand(MIN_HOURS, MAX_HOURS);
  const m = rangeRand(0, 59);
  const s = rangeRand(0, 59);
  const duration = rangeRand(MIN_STAY_TIME, MAX_STAY_TIME);

  while(totalDuration <= THRES_TRIP) {

  }


  console.log(`${h}:${m}:${s}`);
  console.log(duration);
}

function run() {
  let traj = [];
  createStayTimes();
}

run();
