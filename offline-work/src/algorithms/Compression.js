import compress from 'simplify-js';
import merc from 'mercator-projection';

import complicate from '../utils/data';
import { notNaN } from '../utils/utils';

/*
  @THETA: For scaling latitude and longitude due to
  most of the differences between these coordinate values
  are too small, which cause the `merc.fromLatLngToPoint`
  output same values.
*/
const THETA = 100000;

const transformScaling = (point) => ({
  x: point.x * THETA,
  y: point.y * THETA
});

const recoverScaling = (point) => ({
  x: point.x / THETA,
  y: point.y / THETA
});

/*
  @toleracne: [Float], compression rate

  @highQuality: [Boolean], shold use HQ mode or not. HQ mode will
  reduce lots of points in trajectory.

  @complexity: [Number], number of points which stuff into trajectory to
  make it complex enough for compression task.
*/
function compressTraj(trajectory, {tolerance, complexity, highQuality})
{
  const tmpTraj = complicate(trajectory, complexity);
  const complexTraj = tmpTraj
    .map(merc.fromLatLngToPoint)
    .map(transformScaling)
    .filter((p) => notNaN(p.x) && notNaN(p.y));

  const result = compress(complexTraj, tolerance, highQuality)
          .map(recoverScaling)
          .map(merc.fromPointToLatLng);

  return { complexTraj: tmpTraj, result };
}

export default compressTraj;
