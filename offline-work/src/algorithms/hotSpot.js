// import path from 'path';
import _ from 'lodash';
import math from 'mathjs';
import readStayTrajs from '../stayTraj';
import {descSort} from '../utils/utils';


const DEFAULT_INTEREST = 0.0001;
// For floor effect (assume each floor have 40 locations inside)
const DUMMY_NUM_ROOMS = 40;
// Weight for balancing time-effect and floor-effect
const W = 0.7
// Control the endings of iterations of the user-location matrix
const EPSILON = 0.001;

export default function detectHotSpots(stayTrajs) {
  const features = buildFeatures(stayTrajs);
  const result = wrapUp(features);

  return result;
}

function wrapUp(features) {
  const {hotness, activeness, metaData, usersInterests} = features;
  const {users, locations} = metaData;

  const hotSpots = getHotSpots({hotness, locations});
  const activeUsers = getActiveUsers({activeness, users});

  return {hotSpots, activeUsers};
  // return {hotSpots, activeUsers, usersInterests, metaData};
}

function getHotSpots({hotness, locations}) {
  const withIndices = bindIndices(hotness._data);
  const info = withIndices.map(e => ({
    hotness: e.value,
    location: locations[e.idx],
    index: e.idx
  }));
  // info.forEach(e => {
  //   const {building, floor, room} = e.location;
  //   const target = `${building}_${floor}_${room}`;
  //   if (target === '1_1_106' || target === '1_0_001' || target === '1_0_117') {
  //     console.log('hotness', target, e.index, e.hotness);
  //     console.log();
  //   }
  // });
  const hotSpots = descSort(info, ['hotness']);

  return hotSpots;
}

function getActiveUsers({activeness, users}) {
  const withIndices = bindIndices(activeness._data);

  const info = withIndices
    .map(e => ({
      activeness: e.value,
      user: users[e.idx]
    }));

  const activeUsers = descSort(info, ['activeness']);

  return activeUsers
}

function bindIndices(arr) {
  return _.flatten(arr)
          .map((e, idx) => ({idx, value: e}));
}

/*
  Building `Hotness(H)` and `Activeness(A)` feature vectors
  from `Interest of Users(M)` matrix.

  NOTE: the algorithm refer to
  https://link.springer.com/chapter/10.1007/978-3-319-18120-2_13
*/
function buildFeatures(stayTrajs) {
  const usersInterests = extractUserInterests(stayTrajs);
  const metaData = extractMetaData(usersInterests)
  const {users, locations} = metaData;
  //verifyLocationInfo(usersInterests);
  const M = initMatrix(usersInterests, locations);
  let H = initHotness(locations.length);
  let A = initActiveness(users.length);
  let Hprev = [];
  let Aprev = [];

  // DEBUG: 1_1_106 -> 276,   1_0_001 -> 308
  let rounds = 0;
  do {
    rounds += 1;
    Hprev = H;
    Aprev = A;
    H = normalize(updateHotness(M, Hprev));
    console.log(`hotness of 1_0_117  ->  ${H._data[267]}`);
    console.log(`hotness of 1_1_106  ->  ${H._data[275]}`);
    console.log(`hotness of 1_0_001  ->  ${H._data[308]}`);
    console.log();
    A = normalize(updateActiveness(M, Aprev));
  } while(notConverged(H, A, Hprev, Aprev));

  // console.log(`Converge at ${rounds}th rounds`);
  // console.log('-----------------------\n');
  return {hotness: H, activeness: A, metaData, usersInterests};
}

function notConverged(H, A, Hprev, Aprev) {
    return isUpdateNeeded(H, Hprev) || isUpdateNeeded(A, Aprev);
}

function isUpdateNeeded(cur, prev) {
  // return math.sum(
  //   math.subtract(cur, prev).map(Math.abs)
  // );
  const diff = math.subtract(cur, prev);
  return diff._data
          .filter(d => d >= EPSILON)
          .length !== 0;
}

function updateHotness(M, prevHotness) {
  const Mt = math.transpose(M);
  const tmp = math.multiply(Mt, M);
  return math.multiply(tmp, prevHotness);
}

function updateActiveness(M, prevActiveness) {
  const Mt = math.transpose(M);
  const tmp = math.multiply(M, Mt);
  return math.multiply(tmp, prevActiveness);
}

function initHotness(numLocations) {
  const v = Array(numLocations).fill([1 / numLocations]);
  return math.matrix(v);
}

function initActiveness(numUsers) {
  const v = Array(numUsers).fill([1 / numUsers]);
  return math.matrix(v);
}

/*
  Extracting meta data for referencing interest value
  to location by array indices.

  The reason for writing this function is that Matrix(`mathjs`)
  structure should only contains numeric values so that
  we can easily operate(add, sub, mul, div) on it.
*/
function extractMetaData(usersInterests) {
  const locations = getLocations(usersInterests);
  const users = usersInterests.map(e => e.user);
  return {users, locations};
}


/*
  Extracting user interests from (sprase) stay trajectories
*/
function extractUserInterests(stayTrajs) {
  const trajInterests = stayTrajs.map(st => {
    const interests = calcUsersInterests(st.data);
    return {user: st.user, interests};
  });

  const interestGroups = _.groupBy(trajInterests, (st) => st.user);
  const usersInterests = _.map(interestGroups, group => {
    const merged = _.flatten(group.map(e => e.interests));
    return {
      user: group[0].user,
      interests: merged
    }
  });

  return usersInterests;
}

function initMatrix(usersInterests, locations) {
  const intensites = usersInterests
      .map(u => fillEmptyInterests(u, locations))

  return math.matrix(intensites);
}

/*
  Filling up empty record with default interest intensity.
  `Empty` means a user haven't visited that location.
*/
function fillEmptyInterests(userData, locations) {
  // const visited = userData.interests.map(e => e.location);
  // const notVisited = _.differenceWith(locations, visited, _.isEqual);
  const interests = userData.interests;

  const newInterests = locations.map(loc => {
      let intensity = 0;
      const idx = _.findIndex(interests, ['location', loc]);

      if (idx !== -1) {
        intensity = interests[idx].intensity;
      } else {
        intensity = DEFAULT_INTEREST;
      }

      return {location: loc, intensity};
  });

  return newInterests.map(e => e.intensity);
}

/* Calculate every user's interests in location */
function calcUsersInterests(stayTraj) {
  const floor = stayTraj[0].location.floor;
  const floorEffect = floor / DUMMY_NUM_ROOMS;  // preserved
  const locDurations = calcDurations(stayTraj);
  const totalDuration = _.sum(locDurations.map(d => d.avgDuration));

  const interests = locDurations.map(ld => {
    const timeEffect = ld.avgDuration / totalDuration;
    const intensity = W * timeEffect + (1-W) * floorEffect;
    return {location: ld.location, intensity};
  });

  return interests;
}

function calcDurations(traj) {
  const duration = (sp) => {
    const tx = new Date(sp.leaveTime);
    const ty = new Date(sp.arrivalTime);
    return (tx - ty) / 60000;
  };

  const locations = _.uniqWith(traj.map(p => p.location), _.isEqual);

  const locDurations = locations.map(loc => {
    const ds = traj
      .filter(p2 => loc === p2.location)
      .map(duration);

    let avg = 0.0;
    if (ds.length === 1) {
      avg = ds[0];
    } else {
      const total = _.sum(ds);
      avg =  total / ds.length;
    }

    return {avgDuration: avg, location: loc};
  });

  return locDurations;
}

function getUserIds(stayTrajs) {
  return _.sortBy(_.uniq(
    stayTrajs.map(st => st.id.user)
  ));
}

function getLocations(usersInterests) {
  const groupedLocations = usersInterests.map(k => {
    return k.interests.map(v => v.location);
  });

  return _.uniqWith(_.flatten(groupedLocations), _.isEqual);
}

/*
  @m: the matrix(or vector) being normalized
*/
function normalize(m) {
  const max = math.max(m);
  const min = math.min(m);
  return m.map(x => (x - min) / (max - min));
}


/* Below is testing code */
test();

function test() {
  const stayTrajs = readStayTrajs();
  const features = buildFeatures(stayTrajs);
  const summary = wrapUpTest(features);
  const result = showHotSpots(summary);
  verifyResult(result);
}

function wrapUpTest(features) {
  const {hotness, activeness, metaData, usersInterests} = features;
  const {users, locations} = metaData;
  const hotSpots = getHotSpots({hotness, locations});
  const activeUsers = getActiveUsers({activeness, users});

  return {hotSpots, activeUsers, usersInterests, metaData};
}

function showHotSpots({usersInterests, hotSpots, activeUsers, metaData}) {
  const K = 15;
  const topK = hotSpots.slice(0, K);

  console.log(`Top ${K} hot locations:  [location]  --> [hotness value]`);
  for (const elem of topK) {
    const {building, floor, room} = elem.location
    console.log(`${building}_${floor}_${room} -->  ${elem.hotness}`);
    console.log();
  }

  return {usersInterests, hotSpots, locations: metaData.locations};
}

function verifyResult({usersInterests, hotSpots, locations}) {
  console.log();
  console.log();

  // Users Activeness
  let tmpA = usersInterests.map(user => {
    const total = _.sum(user.interests.map(e => e.intensity))
    const visits = user.interests.map(e => e.location);
    return {id: user.user, total, visits};
  })

  const activeness = normalize(tmpA.map(e => e.total));
  tmpA = tmpA.map((e, idx) => {
    return Object.assign({}, e, {total: activeness[idx]});
  });
  const rawA = descSort(tmpA, ['total']);



  // Locations Hotness
  let tmpH = locations.map(loc => {
    const matches = usersInterests.map(u => {
      const interests = _.without(
        u.interests.filter(v => _.isEqual(v.location, loc))
        , undefined
      );
      return {uid: u.user, interests};
    })
    .filter(e => e.interests.length > 0);

    const ints = _.flattenDeep(matches.map(m => m.interests.map(e => e.intensity)));
    const total = _.sum(ints);

    const intOfUsers = matches.map(m => {
      return {
        uid: m.uid,
        ints: _.flattenDeep(m.interests.map(e => e.intensity))
      };
    });

    return {location: loc, total, intOfUsers};
  });

  // const hotness = normalize(tmpH.map(e => e.total));
  const hotness = tmpH.map(e => e.total);
  // console.log(tmpH.map(e => e.total));
  // console.log(hotness);

  tmpH = tmpH.map((e, idx) => {
    return Object.assign({}, e, {total: hotness[idx]});
  });

  const rawH = descSort(tmpH, ['total']);



  console.log('Active users:');
  rawA.forEach(a => {
    console.log(`user #${a.id}  -->  ${a.total}`)
    const visits = a.visits.map(v => `${v.building}_${v.floor}_${v.room}`);
    console.log(`visit ${visits.length} locations`);
    console.log();
  });

  console.log();
  console.log();

  console.log('Hot locations ([Building_Floor_Room]  --> [hotness value]):');
  rawH.forEach(h => {
    const {building, floor, room} = h.location;
    const location = `${building}_${floor}_${room}`;
    console.log(`${location}  -->  ${h.total}`)
    for (const e of h.intOfUsers) {
      console.log(`    user #${e.uid} -> ${e.ints}`);
    }
    console.log();
  });

  // findRevisted(locations);
}

function findRevisted(locations) {
  readStayTrajs()
    .then(trajs => {
      const stayTrajs = trajs.map(t => {
        return t.data.map(v => {
                 const {building, floor} = t.id;
                 return {building, floor, room: v.location.id};
               });
      })

      locations.forEach(loc => {
        stayTrajs.forEach(st => {
          const matches = st.filter(p => _.isEqual(p, loc));

          if (matches.length > 1) {
            console.log(matches.length);
            console.log(matches);
            console.log('Found revisited point.');
          }

          // if (matches.length === 1) {
          //   console.log('Only once');
          // }
        });
      });
    })
    .catch(err => {
      if (err) { console.error(err); }
    });
}

function verifyLocationInfo(userInterests) {
  const groupedLocations = userInterests.map(k => {
    return k.interests.map(v => v.location);
  });

  userInterests.map(userData => {
    console.log(userData.interests.length);
  });

  const locations = _.uniqWith(_.flatten(groupedLocations), _.isEqual);
  const building0 = locations.filter(e => e.building === 0);
  const building1 = locations.filter(e => e.building === 1);
  const building2 = locations.filter(e => e.building === 2);
  const totalLocations = locations.length;

  printHaveVistedInBuilding(0, locations, groupedLocations);
  printHaveVistedInBuilding(1, locations, groupedLocations);
  printHaveVistedInBuilding(2, locations, groupedLocations);
}

function printHaveVistedInBuilding(buildingId, allLocations, groupedLocations) {
  const header = `Building ${buildingId}`;
  const locations = allLocations.filter(e => e.building === buildingId);
  const total = locations.length;

  console.log(header);
  console.log(total);

  groupedLocations.map(k => {
    const k1 = k.filter(e => e.building === buildingId);
    const rate = k1.length / total;
    if (rate > 0) {
      console.log(`    ${k1.length}, ${rate*100} %`);
    }
  });
  console.log();
}
