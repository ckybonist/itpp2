import _ from 'lodash'
import calcSpeed from '../utils/speed'


const MAX_SPEED = 12.4;  // m/s

export default function filterNoise(traj, THRES_SPEED) {
  if (THRES_SPEED > MAX_SPEED) {
    console.error('Too fast for this speed threshold.');
  }

  const normals = [traj[0]];  // assume first point is correct
  let outliers = [];

  for (let i = 1; i < traj.length; i++) {
    const p1 = _.last(normals);
    const p2 = traj[i];
    const speed = calcSpeed(p1, p2, 'walking');

    if (speed <= THRES_SPEED) {
      normals.push(p2);
      //console.log(`Normal: ${normals.length}`);
    } else {
      const show = (p) => {
        return `(${p.coordinate.latitude}, ${p.coordinate.longitude})   ${p.time.toGMTString()}`;
      };

      console.log(`--> found outlier p${i}:`);
      console.log(`.... speed between p${i-1} and p${i}: ${speed} m/s`);
      console.log(`.... p${i-1}:   ${show(p1)}`);
      console.log(`.... p${i}:   ${show(p2)}`);
      // console.log();

      const {latitude, longitude} = p2.coordinate;
      outliers.push({latitude, longitude});
    }
  }

  outliers = _.uniqWith(outliers, _.isEqual);

  return {normals, outliers};
}
