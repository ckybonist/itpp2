import path from 'path';
import _ from 'lodash';
import readStayTrajs from '../stayTraj';
import {descSort, range} from '../utils/utils';


const NUM_BUILDINGS = 3;
const NUM_FLOORS = 4;

run();

function run() {
  const stayTrajs = readStayTrajs().map(t => t.data.map(e => e.location));
  showRevisited(stayTrajs);
}

function showRevisited(stayTrajs) {
  const revisits = descSort(findRevisted(stayTrajs), ['freq']);
  for (const rv of revisits) {
    console.log(rv);
  }
  // summary(revisits);
  // for (const rv of revisits) {
  //   console.log(rv);
  // }

  // console.log();
  // console.log();
  // showRevisitsEachBuilding(revisits);
}

function summary(revisits) {
  const freqGroups = _.groupBy(revisits, 'freq');
  const result = [];

  _.forEach(freqGroups, (points, freq) => {
    const total = points.length;
    const items = [];
    for (const bid of range(NUM_BUILDINGS)) {
      for (const fid of range(NUM_FLOORS)) {
        const key = {building: bid, floor: fid};
        const targets = findPointsAt(points, key);
        if (freq === '1') {
          console.log(key, targets.length, total);
        }
        if (!_.isEmpty(targets)) {
          items.push({location: key, ratio: targets.length / total, counts: targets.length, total: total});
        }
      }
    }
    result.push({freq, items});
  });

  for (const res of result) {
    console.log(`Freq: ${res.freq}`);
    const mostRevisited = _.maxBy(res.items, 'ratio');
    console.log(descSort(res.items, ['ratio']));
    // if (res.freq == 3) {
    //   console.log(res.items);
    // } else {
    //   console.log(mostRevisited);
    // }
    console.log();
  }
}

function findPointsAt(points, location) {
  return points.filter(p => {
    const {building, floor} = p.location;
    return _.isEqual({building, floor}, location)
  });
}

function showRevisitsEachBuilding(revisits) {
  for (const bid of range(NUM_BUILDINGS)) {
    const target = revisits.filter(e => e.location.building === bid);
    const totalFreqs = _.sum(target.map(e => e.freq));
    console.log(`Building ${bid}: ${totalFreqs} revisits.`);
  }
}

function findRevisted(stayTrajs) {
  const locations = getAllLocations(stayTrajs);
  // const trajs = stayTrajs.map(st => getLocation(st));

  const result = locations
    .map(loc => stayTrajs.map(t => t.filter(p => _.isEqual(p, loc))))
    .map(matches => _.flatten(matches.filter(m => m.length > 0)))
    .map(matches => {
      return {
        location: matches[0],
        freq: matches.length - 1
      };
    })
    .filter(e => e.freq > 0);

  return result;
}

function getAllLocations(stayTrajs) {
  // const tmp = stayTrajs.map(getLocation);
  return _.uniqWith(_.flatten(stayTrajs), _.isEqual);
}

function getLocation(traj) {
  return traj.data.map(p => {
    const {building, floor} = traj.id;
    return {building, floor, room: p.location.id};
  });
}
