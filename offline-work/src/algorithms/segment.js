import _ from 'lodash';

function createSplitPoints(specialPoints, numPoints) {
  const pushHead = () => {
    const sp = specialPoints[0];
    splitPoints.push([0, sp.startIndex])
  };
  const pushTail = () => {
    const sp = _.last(specialPoints);
    splitPoints.push([sp.endIndex+1, numPoints])
  };

  const pushMiddle = () => {
    const fn = (cur, next) => splitPoints.push(
      [cur.endIndex+1, next.startIndex]
    );

    specialPoints.forEach((sp, idx) => {
      const next = specialPoints[idx+1];

      if (specialPoints.length > 1 && idx === 0) {
        fn(sp, next);
      }

      if (idx > 1 && idx < specialPoints.length-1) {
        const next = specialPoints[idx+1].startIndex;
        fn(sp, next);
      }
    });
  };

  const splitPoints = [];

  pushHead();
  if (specialPoints.length > 1) {
    console.log('pushMiddle()');
    pushMiddle();
  }
  pushTail();

  return splitPoints;
}

export default function splitToSegments(trajectory, specialPoints) {
  if (specialPoints.length === 0) return [];

  const splitPoints = createSplitPoints(specialPoints, trajectory.length);

  const traj = trajectory.map(
    (point, index) => ({ index, data: point })
  );

  return splitPoints
    .map(sp => traj.slice(sp[0], sp[1]))
    .filter(seg => seg.length != 0);
}
