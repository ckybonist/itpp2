let distance = require('google-distance-matrix');
let origins = ['39.992168957037286,-0.066534156697710323'];
let destinations = ['39.99189380021565,-0.06590444316861951'];

distance.mode('walking');
distance.avoid('tolls');

distance.matrix(destinations, origins, function (err, result) {
    if (!err) {
        const element = result.rows[0].elements[0];
        const distance = element.distance.text;
        console.log(distance);
    }
});
