const gmapClient = require('@google/maps')
    .createClient({ key: 'AIzaSyADfx2tAm2X9ikfY8D5YN5DbQ6sNcmlxE4' });

const origin = ['39.992168957037286', '-0.066534156697710323'];
const destination = ['39.99189380021565', '-0.06590444316861951'];
const query = {
    origin: origin,
    destination: destination,
    mode: 'walking',
    //avoid: 'tolls, highways, ferries',
    //optimize: true
};

gmapClient.directions(query, (err, response) => {
    if (err) { console.log(err); }
    const result = response.json;
    const route = result.routes[0].legs[0];
    console.log(route.distance);
});
