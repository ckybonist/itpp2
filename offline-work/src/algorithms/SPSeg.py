#!/usr/bin/env python3
# -*- coding: utf-8 -*-


"""
    Assume the structure of stay point
    is a dictionary with such fields:
        {
            startIndex: <Integer>,
            endIndex: <Integer>,
            roomID: <Integer>,
            arrivalTime: <Float>,
            leaveTime: <Float>,
        }

"""
def splitTrajectory(trajectory, stayPoints):
    splitBoundaries = createSplitBoundaries(stayPoints, len(trajectory))

    sliceTrajBy = lambda sb: trajectory[sb[0] : sb[1]]
    segments = map(sliceTrajBy, splitBoundaries)

    segments = filter(None, segments)  # filter out empty segment

    return list(segments)

"""
    numPoints: number of points in trajectory
"""
def createSplitBoundaries(stayPoints, numPoints):
    splitBoundaries = []

    for (i, sp) in enumerate(stayPoints):
        boundary = (0, 0)

        if (i < len(stayPoints)-1):
            if (idx == 0):
                boundary = (0, sp["startIndex"])

            nextSp = stayPoints[i+1]
            bonudary = (sp["endIndex"]+1, nextSp["startIndex"])
        else:
            boundary = (sp["endIndex"]+1, numPoints)

        splitBoundaries.append(boundary)

    return splitBoundaries
