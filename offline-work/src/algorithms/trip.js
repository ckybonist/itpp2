import _ from 'lodash';


const MINUTES_A_DAY = 1440;

function calcTrajDuration(points) {
    const tmp = Math.abs(_.last(points).time - points[0].time);
    return Math.floor10(tmp / 60000, -3);
}

function splitToTrips(trajectories, MAX_LENGTH, MAX_DURATION) {
    console.log('\n\n* Trip Segmentation');
    const trips = [];
    const pushTrip = (traj, points) => {
        const tmp = Object.assign({}, traj, {points});
        trips.push(tmp);
    };
    const checkDiffDay = (d1, d2) => d1.time.getDate() != d2.time.getDate();

    for (const traj of trajectories) {
        const {building, floor, uid, pid, points} = traj;
        const duration = calcTrajDuration(points);

        if (duration > MAX_DURATION) {
          console.log('---------------------------')
            console.log(`--> found trajectory with long duration: ${duration} minutes`);
            console.log(`--> trajectory length: ${points.length}`);
            // console.log(`User: ${uid}, Phone: ${pid}`);
            // console.log(`At Building: ${building}, Floor: ${floor}\n`);
            console.log(`\n--> split to following trips:`);

            let p1 = {idx: 0, data: points[0]};
            let tripCount = 0;

            points.forEach((p2, idx) => {
                if (idx === 0)
                    return;

                const duration = (p2.time - p1.data.time) / 60000;
                const atDiffDay = checkDiffDay(p1.data, p2);
                const atFinalIter = idx === points.length - 1;

                if (atDiffDay || atFinalIter) {
                    const trip = (atFinalIter)
                        ? points.slice(p1.idx)
                        : points.slice(p1.idx, idx);

                    tripCount += 1;
                    console.log(`\ntrip #${tripCount}:`);

                    if (trip.length >= MAX_LENGTH) {
                        pushTrip(traj, trip);
                        const startAt = trip[0].time.toGMTString();
                        const endAt = _.last(trip).time.toGMTString();
                        console.log(`   start at: ${startAt}`);
                        console.log(`   end at:   ${endAt}`);
                        console.log(`   trip length: ${trip.length}`);
                        console.log(`   trip duration: ${calcTrajDuration(trip)} minutes\n`);
                    } else {
                        console.log(`   This trip is too short: ${trip.length} points\n`);
                    }

                    p1 = {idx, data: p2};
                }
            });
          console.log('---------------------------')
          
        } else {
            pushTrip(traj, points);
        }
    }

    return trips;
}

export default splitToTrips;
