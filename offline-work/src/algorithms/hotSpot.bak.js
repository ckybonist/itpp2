const path = require('path');
const _ = require('lodash');
const Promise = require('bluebird');
const fs = Promise.promisifyAll(require('fs'));
const math = require('mathjs');


const DEFAULT_INTEREST = 0.0001;
// For floor effect (assume each floor have 40 locations inside)
const DUMMY_NUM_ROOMS = 40;
// weight for balancing time-effect and floor-effect
const W = 0.7
const EPSILON = 0.00001;
// control the endings of iterations on the user-location matrix

test();

// export default function run() {
//   return readStayTrajs()
//     .then(buildFeatures)
//     .then(showHotSpotsInfo)
//     .catch(err => {
//       if (err) { console.error(err); }
//     });
// }

function test() {
  readStayTrajs()
    .then(buildFeatures)
    .then(showHotSpotsInfo)
    .then(verifyResult)
    .catch(err => {
      if (err) { console.error(err); }
    });
}

function readStayTrajs() {
  const fpath = path.resolve(__dirname, '../../logs/stayTrajs.json');
  return fs.readFileAsync(fpath)
           .then(data => JSON.parse(data));
}

function showHotSpotsInfo({hotness, usersInterests, locations, metaData}) {
  const k = 3;
  console.log(`Top ${k} locations:`);

  const locs = _.flatten(hotness._data)
                .map((e, idx) => ({idx, value: e}));

  const topK = _.reverse(_.sortBy(locs, ['value']))
                .slice(0, k);
  const hotLocations = topK.map(e => locations[e.idx]);

  topK.forEach(e => {
    console.log(locations[e.idx], e.value);
  });
  console.log('-------------------------\n');

  return {usersInterests, hotLocations, locations};
}

// function verifyResult({usersInterests, hotLocations, locations}) {
//   console.log('Verify hot spots:');
//   const summary = locations.map(loc => {
//   // const summary = hotLocations.map(loc => {
//     let numUsers = 0;
//     let interestSum = 0.0;
//     const ids = [];
//
//     usersInterests.forEach(u => {
//       const matches = _.filter(u.interests, ['location', loc]);
//       // matches.forEach(e => console.log(e.location));
//
//       if (matches.length > 0) {
//         numUsers += 1;
//         interestSum += _.sum(matches.map(e => e.intensity));
//         ids.push(u.user);
//       }
//     });
//
//     return {userIds: ids, location: loc, numUsers, interestSum};
//   });
//
//   _.reverse(_.sortBy(summary, ['interestSum', 'numUsers']))
//     .forEach(s => {
//       console.log(s.location);
//       console.log(`Total users: ${s.numUsers}    Total Interests: ${s.interestSum}`);
//       console.log(s.userIds);
//       s.userIds.forEach(id => {
//         const tmp = usersInterests.filter(e => e.user === id);
//         const total = _.sum(tmp[0].interests.map(e => e.intensity));
//         console.log(total);
//       });
//       console.log();
//     });
// }

function verifyResult({usersInterests, hotLocations, locations}) {
  console.log('Verify hot spots:');


  const rawA = _.reverse(_.sortBy(
    usersInterests.map(user => {
      const total = _.sum(user.interests.map(e => e.intensity))
      return {id: user.user, total};
    })
    , ['total']
  ));

  const tmpH = locations.map(loc => {
    const matches = usersInterests.map(u => {
      const interests = _.without(
        u.interests.filter(v => _.isEqual(v.location, loc))
        , undefined
      );
      return {uid: u.user, interests};
    })
    .filter(e => e.interests.length > 0);

    const users = matches.map(m => m.uid);
    const total = _.sum(
      matches.map(m => m.interests.map(e => e.intensity))
    );

    return {location: loc, total, users};
  });
  const rawH = _.reverse(_.sortBy(tmpH, ['total']));

  console.log('A user for locations:');
  rawA.forEach(a => console.log(`user #${a.id}  -->  ${a.total}`));
  console.log();

  console.log('A location for users:');
  console.log('Building_Floor_Room');
  rawH.forEach(h => {
    const {building, floor, room} = h.location;
    const location = `${building}_${floor}_${room}`;
    console.log(`${location}  -->  ${h.total}`)
    console.log(h.users);
    console.log();
  });

  // findRevisted(locations);
}

function findRevisted(locations) {
  readStayTrajs()
    .then(trajs => {
      const stayTrajs = trajs.map(t => {
        return t.data.map(v => {
                 const {building, floor} = t.id;
                 return {building, floor, room: v.location.id};
               });
      })

      locations.forEach(loc => {
        stayTrajs.forEach(st => {
          const matches = st.filter(p => _.isEqual(p, loc));

          if (matches.length > 1) {
            console.log(matches.length);
            console.log(matches);
            console.log('Found revisited point.');
          }

          // if (matches.length === 1) {
          //   console.log('Only once');
          // }
        });
      });
    })
    .catch(err => {
      if (err) { console.error(err); }
    });
}

/*
  Building `Hotness(H)` and `Activeness(A)` feature vectors
  from `Interest of Users(M)` matrix.

  NOTE: the algorithm refer to
  https://link.springer.com/chapter/10.1007/978-3-319-18120-2_13
*/
function buildFeatures(stayTrajs) {
  // console.log(`Stay trajs length: ${stayTrajs.length}`);
  // const totalStayPoints = _.sum(stayTrajs.map(st => st.data.length));
  // console.log(totalStayPoints);

  const usersInterests = extractUserInterests(stayTrajs);
  const metaData = extractMetaData(usersInterests)
  //verifyLocationInfo(usersInterests);
  const locations = getLocations(usersInterests);
  const numUsers = usersInterests.length;

  const M = initMatrix(usersInterests, locations);
  let H = initHotness(locations.length);
  let A = initActiveness(numUsers);
  let Hprev = [];
  let Aprev = [];

  let rounds = 0;

  do {
    rounds += 1;
    Hprev = H;
    Aprev = A;
    H = normalize(updateHotness(M, Hprev));
    A = normalize(updateActiveness(M, Aprev));
  } while(notConverged(H, A, Hprev, Aprev));

  console.log(`Converge at ${rounds}th rounds`);
  console.log('-----------------------\n');

  return {
    hotness: H,
    activeness: A,
    usersInterests,
    locations,
    metaData
  };
}

function notConverged(H, A, Hprev, Aprev) {
  const Herr = calcErr(H, Hprev);
  const Aerr = calcErr(A, Aprev);
  // console.log(`${Herr}   ${Aerr}`);
  return Herr >= EPSILON || Aerr >= EPSILON;
}

function calcErr(cur, prev) {
  return math.sum(
    math.subtract(cur, prev).map(Math.abs)
  );
}

function updateHotness(M, prevHotness) {
  const Mt = math.transpose(M);
  const tmp = math.multiply(Mt, M);
  return math.multiply(tmp, prevHotness);
}

function updateActiveness(M, prevActiveness) {
  const Mt = math.transpose(M);
  const tmp = math.multiply(M, Mt);
  return math.multiply(tmp, prevActiveness);
}

function initHotness(numLocations) {
  const v = Array(numLocations).fill([1 / numLocations]);
  return math.matrix(v);
}

function initActiveness(numUsers) {
  const v = Array(numUsers).fill([1 / numUsers]);
  return math.matrix(v);
}

/*
  Extracting meta data for referencing interest value
  to location by array indices.
  The reason of writing this function is that Matrix(`mathjs`)
  structure should only contains numeric values so that
  we can easily operate(add, sub, mul, div) on it.
*/
function extractMetaData(userInterests) {
  return userInterests.map(r1 => {
    const {user, interests} = r1;
    const traj = interests.map(r2 => r2.location);
    return {user, traj}
  });
}

function findLocationByIndex(metaData, userIndex, trajIndex) {
  const traj = metaData[userIndex].traj;
  return traj[trajIndex];
}

/*
  Extracting user interests from (sprase) stay trajectories
*/
function extractUserInterests(stayTrajs) {
  const trajInterests = stayTrajs.map(st => {
    const { user, building, floor } = st.id;
    const interests = calcUsersInterests(st.data, st.id)
      .map(e => {
        const {room, intensity} = e;
        const location = {building, floor, room};
        return {location, intensity};
      });

    return {user, interests};
  });

  const interestGroups = _.groupBy(trajInterests, (st) => st.user);
  const usersInterests = _.map(interestGroups, group => {
    const merged = _.flatten(group.map(e => e.interests));
    return {
      user: group[0].user,
      interests: merged
    }
  });

  return usersInterests;
}

function initMatrix(usersInterests, locations) {
  const intensites = usersInterests
      .map(u => fillEmptyInterests(u, locations))

  return math.matrix(intensites);
}

/*
  Filling up empty record with default interest intensity.
  `Empty` means a user haven't visited that location.
*/
function fillEmptyInterests(userData, locations) {
  // const visited = userData.interests.map(e => e.location);
  // const notVisited = _.differenceWith(locations, visited, _.isEqual);
  const interests = userData.interests;

  const newInterests = locations.map(loc => {
    let intensity = 0;
    const idx = _.findIndex(interests, ['location', loc]);

    if (idx !== -1) {
      intensity = interests[idx].intensity;
    } else {
      intensity = DEFAULT_INTEREST;
    }

    return {location: loc, intensity};
  });

  return newInterests.map(e => e.intensity);
  // const defaultInterests = notVisited.map(location => {
  //   return {location, intensity: DEFAULT_INTENSITY};
  // });
  // const interests = userData.interests.concat(defaultInterests);
  //
  // return Object.assign({}, userData, {interests});
}

/* Calculate every user's interests in location */
function calcUsersInterests(stayTraj, trajId) {
  const locDurations = calcDurations(stayTraj);
  const floor = trajId.floor;
  const floorEffect = floor / DUMMY_NUM_ROOMS;  // preserved
  const totalDuration = _.sum(locDurations.map(d => d.avgDuration));

  const interests = locDurations.map(ld => {
    const timeEffect = ld.avgDuration / totalDuration;
    const intensity = W * timeEffect + (1-W) * floorEffect;
    return {room: ld.room, intensity};
  });

  return interests;
}

function calcDurations(stayTraj) {
  const duration = (sp) => {
    const tx = new Date(sp.leaveTime);
    const ty = new Date(sp.arrivalTime);
    return (tx - ty) / 60000;
  };

  const roomIds = getRoomIds(stayTraj);

  const locDurations = roomIds.map(rid => {
    const ds = stayTraj
      .filter(p => rid === p.location.id)
      .map(duration);

    let avg = 0.0;
    if (ds.length === 1) {
      avg = ds[0];
    } else {
      const total = _.sum(ds);
      avg =  total / ds.length;
    }

    return { avgDuration: avg, room: rid };
  });

  return locDurations;
}

function getUserIds(stayTrajs) {
  return _.sortBy(_.uniq(
    stayTrajs.map(st => st.id.user)
  ));
}

function getRoomIds(traj) {
  return _.uniq(
    traj.map(p => p.location.id)
  );
}

function getLocations(userInterests) {
  const groupedLocations = userInterests.map(k => {
    return k.interests.map(v => v.location);
  });
  return _.uniqWith(_.flatten(groupedLocations), _.isEqual);
}

function verifyLocationInfo(userInterests) {
  const groupedLocations = userInterests.map(k => {
    return k.interests.map(v => v.location);
  });

  userInterests.map(userData => {
    console.log(userData.interests.length);
  });

  const locations = _.uniqWith(_.flatten(groupedLocations), _.isEqual);
  const building0 = locations.filter(e => e.building === 0);
  const building1 = locations.filter(e => e.building === 1);
  const building2 = locations.filter(e => e.building === 2);
  const totalLocations = locations.length;

  printHaveVistedInBuilding(0, locations, groupedLocations);
  printHaveVistedInBuilding(1, locations, groupedLocations);
  printHaveVistedInBuilding(2, locations, groupedLocations);
}

function printHaveVistedInBuilding(buildingId, allLocations, groupedLocations) {
  const header = `Building ${buildingId}`;
  const locations = allLocations.filter(e => e.building === buildingId);
  const total = locations.length;

  console.log(header);
  console.log(total);

  groupedLocations.map(k => {
    const k1 = k.filter(e => e.building === buildingId);
    const rate = k1.length / total;
    if (rate > 0) {
      console.log(`    ${k1.length}, ${rate*100} %`);
    }
  });
  console.log();
}

/*
  @m: the matrix(or vector) being normalized
*/
function normalize(m) {
  const max = math.max(m);
  const min = math.min(m);
  return m.map(x => (x - min) / (max - min));
}
