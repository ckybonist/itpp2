import _ from 'lodash';
import detectHotSpots from './hotSpot';
import readStayTrajs from '../stayTraj';
import {descSort} from '../utils/utils';


// Minimum order of hot-spots for determining
// how HOT the route is.
const HOT_TOLERANCE = 10;
// Minimum points in route
const MIN_ROUTE_POINTS = 2;

run();

function run() {
  const stayTrajs = readStayTrajs();
  const candidates = detectHotSpots(stayTrajs).hotSpots;
  const hotRoutes = detectHotRoutes({stayTrajs, candidates});

  show(hotRoutes);

  return hotRoutes;
}

function show(hotRoutes) {
  for (const route of hotRoutes) {
    const {data, freq, totalHotness} = route;
    const routeInfo = data.map(e => {
      const where = locationText(e.location);
      const when = new Date(e.arrivalTime).toGMTString();
      return {where, when};
    });

    console.log(routeInfo);
    console.log(`Frequency: ${freq}`);
    console.log(`Total hotness: ${totalHotness}`);
    console.log();
    console.log();
  }
}

function detectHotRoutes({stayTrajs, candidates}) {
  const hotSpots = candidates.slice(0, HOT_TOLERANCE);
  const trajs = stayTrajs.map(st => st.data);
  let routes = trajs
    .map(t => buildRoutes(t, hotSpots))
    .filter(enoughPoints);
  routes = countRoute(routes);
  routes = routes.map(r => {
    const totalHotness = sumHotness(r.data, hotSpots)
    return Object.assign(r, {totalHotness});
  });

  const hotRoutes = descSort(routes, ['freq', 'totalHotness']);

  return hotRoutes;
}

function locationText({building, floor, room}) {
  return `Building: ${building}  Floor: ${floor}  Room: ${room}`;
}

function countRoute(routes) {
  const myRoutes = routes.map(r => r.map(p => p.location));
  const freqs = myRoutes.map(rx => {
    const targets = myRoutes.filter(ry => _.isEqual(rx, ry));
    return targets.length;
  });

  return _.zip(routes, freqs)
          .map(e => ({data: e[0], freq: e[1]}));
}

function sumHotness(route, hotSpots) {
  return _.sum(route.map(p => {
    const target = _.find(hotSpots, ['location', p.location]);
    return target.hotness;
  }))
}

function buildRoutes(traj, hotSpots) {
  return traj.filter(
    p => isHotSpot(hotSpots, p.location)
  );
}

function isHotSpot(hotSpots, location) {
  const index = _.findIndex(hotSpots, ['location', location])
  return index !== -1;
}

function enoughPoints(route) {
  return route.length >= MIN_ROUTE_POINTS
}
