import mongoose from 'mongoose';
mongoose.Promise = global.Promise;

import _ from 'lodash';

import { MetaData, Trajectory } from './trajectory.model';



mongoose.connect('mongodb://localhost/Analysis');
const db = mongoose.connection;
db.on('error', console.error.bind(console, 'connection error...'));
// test();

export default function getTrajectories(query, fields) {
  return Trajectory.find(query, fields);
}

export function getMetaData() {
  return MetaData.find({}, { _id: 0, trajIDs: 1}).exec();
}

function test() {
  const query = {
    uid: 13,
    'building': 1,
    'floor': 1,
  };

  console.log(query);

  getTrajectories(query)
    .then(data => {
      const traj = _.sortBy(data[0].points, p => p.time);
      traj.forEach(point => {
        const timestr = point.time.toGMTString();
        const { latitude, longitude } = point.coordinate;
        console.log(`${timestr}, ${latitude}, ${longitude}`);
      });

      mongoose.disconnect();
    })
    .catch(err => {
      console.error(err);
    })
}
