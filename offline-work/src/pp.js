import fs from 'fs';
import path from 'path';
import mongoose from 'mongoose';
import Promise from 'bluebird';
import _ from 'lodash';

import getTrajs from './db';
import splitToTrips from './algorithms/trip';
import filterNoise from './algorithms/noiseFiltering';
import detectStayPoints from './algorithms/spd';
import splitToSegments from './algorithms/segment';
import compressTraj from './algorithms/compression';
import {dateTimeStr} from './utils/utils';
import config from './config';
import myTrajs from './myTrajs';


const THRES_TRIP_LENGTH = 20;
const MINUTES_A_DAY = 1440;
const gStayTrajs = [];
let numTrips = 0;
let trajHaveSP = 0;
let round = 0;


function sortData(trajectories) {
    return trajectories.map(cursor => {
        const traj = cursor._doc;
        const points = _.sortBy(traj.points, p => p.time);
        return Object.assign({}, traj, {points});
    });
}

function calcTrajDuration(points) {
    const tmp = Math.abs(_.last(points).time - points[0].time);
    return Math.floor10(tmp / 60000, -3);
}

class Preprocesser {
    constructor({uid, building, floor, points}) {
        this.uid = uid;
        this.floor = floor;
        this.building = building;

        this.points = points; // points of trajectory

        this.cleanPoints = [];
        this.stayPoints = [];
    }

    start() {
      const duration = calcTrajDuration(this.points);
      //if (duration < MINUTES_A_DAY) return;

      console.log('\n=================== Start PP ==================')
      console.log('Meta Info:');
      console.log(`--> User: ${this.uid}, Building: ${this.building}, Floor: ${this.floor}`);
      console.log(`--> Length: ${this.points.length} points in trajectory`);
      console.log(`--> Duration: ${duration} minutes\n`);

      this.runNf(); // Noise Filtering
      this.runSpd(); // Stay Point Detection
      this.runSeg(); // Segmentation
      if (this.uid === 9996) {
        this.runCompression();
      }
      console.log('=================== End PP ====================\n\n')

      round += 1;
      if (round === numTrips) {
          console.log(`${trajHaveSP} trajectories contains stay points.`);
          this.avgStayTimePerBuilding();
          this.avgStayTimePerFloors();
          mongoose.disconnect();
      }
    }

    // sortTrajByTime() {
    //     this.points = _.sortBy(this.points, p => p.time);
    // }

    removeDuplicatePoints(points) {
      const isDuplicate = (px, py) => (
        (px.latitude === py.latitude) && (px.longitude === py.longitude)
      );

      const lastIdx = points.length - 1;

      return points.map((px, idx) => {
        const py = this.points[idx + 1];

        if (idx === lastIdx) {
          return px;
        } else if (!isDuplicate(px, py)) {
          return px;
        }
      }).filter(_.isObject);

      // const origNumPoints = points.length;
      //const reduceRatio = Math.abs(origNumPoints - this.points.length) / origNumPoints * 100;
      //console.log(`--> ${this.points.length} points left after removing duplicates`);
      //console.log(`--> reducing ratio: ${reduceRatio}\n`);
    }

    runNf() {
        console.log('\n\n--> Start Noise Filtering...');

        const THRES_SPEED = config.nf.speed;
        const result = filterNoise(this.points, THRES_SPEED);
        this.cleanPoints = result.normals;

        const numOutliers = result.outliers.length;
        const info = numOutliers > 0
            ? `--> filter out ${numOutliers} outliers`
            : '--> no outliers'
        // console.log(`${info}\n`);
    }

    getStayTime(sp) {
        return (sp.leaveTime - sp.arrivalTime) /1000
    }

    sumTime(stayPoints) {
        return stayPoints.map(this.getStayTime).reduce((a, b) => a + b)
    };

    avgStayTimePerFloors() {
        const numBuildings = 3;
        const floors = [4, 4, 5];

        const infos = gStayTrajs.map(sp => {
            const {id, data} = sp;
            const numPoints = data.length;
            const totalTime = (numPoints > 1)
                ? this.sumTime(data)
                : this.getStayTime(data[0]);
            const avgTime = totalTime / numPoints;
            return {id, avgTime, numPoints};
        });

        const sorted = _.sortBy(infos, ['id.building', 'id.floor']);
        for (const e of sorted) {
            console.log(`(Building: ${e.id.building}, Floor: ${e.id.floor}) => ${e.avgTime} seconds`);
        }
    }

    avgStayTimePerBuilding() {
        console.log('\n\n');

        const NUM_BUILDING = 3;

        for (let bid = 0; bid < NUM_BUILDING; bid++) {
            const stayPoints = _.flattenDeep(
              gStayTrajs
              .filter(e => e.id.building === bid)
              .map(e => e.data)
            );

            if (stayPoints.length === 0) {
                console.log(`--> no stay points in Building ${bid}\n`);
                continue;
            }

            const numSp = stayPoints.length;
            const totalTime = (numSp > 1)
                ? this.sumTime(stayPoints)
                : this.getStayTime(stayPoints[0]);
            const avgTime = totalTime / numSp;
            console.log(`--> averge stay time of building ${bid}: ${avgTime} seconds\n`);
        }
    }

    runSpd() {
        console.log('\n\n--> Start Stay Point Detection...');

        const THRES_TIME = config.spd.stayTime;
        const {stayPoints, stayTimes} = detectStayPoints(this.cleanPoints, THRES_TIME);
        this.stayPoints = stayPoints;

        if (stayPoints.length > 0) {
          console.log(`--> Found ${stayPoints.length} stay points`);

          trajHaveSP += 1;

          const id = {
            user: this.uid,
            building: this.building,
            floor: this.floor
          };

          gStayTrajs.push({id, data: stayPoints});

          for (const sp of stayPoints) {
              const {arrivalTime, leaveTime, location, startIndex, endIndex} = sp;
              const arrivalAt = dateTimeStr(arrivalTime);
              const leaveAt = dateTimeStr(leaveTime);
              let duration = Math.abs(sp.arrivalTime - sp.leaveTime) / (1000 * 60);
              duration = Math.floor10(duration, -3);

              console.log('----------------------------');
              console.log(`  location:  room #${location.id}`);
              console.log(`  arrival at:  ${arrivalAt}`);
              console.log(`  leave at:    ${leaveAt}`);
              console.log(`  start index: ${startIndex}`);
              console.log(`  end index:   ${endIndex}`);
              console.log('----------------------------');

              // test MR hot-locations
              const locTag = `${this.building}_${this.floor}_${location.id}`;
              if (locTag === '1_0_001' || locTag === '1_1_106') {
                const duration = (leaveTime - arrivalTime) / 1000;
                console.log(`${locTag} --> ${duration} s`);
              }
          }
        } else {
          console.log('--> stay points not found');
        }

        console.log();
    }

    runSeg() {
        console.log('--> Start Segmentation...');
        const segments = splitToSegments(this.cleanPoints, this.stayPoints);
        console.log(`--> Number of segments: ${segments.length}`);
        for (const seg of segments) {
            const start = seg[0].index;
            const last = _.last(seg).index;
            console.log('----------------------------');
            console.log(`Segment from point ${start} to ${last}:`);

            for (const point of seg) {
                const data = point.data;
                const {latitude, longitude} = data.coordinate;
                const time = data.time.toGMTString();
                console.log(`    coordinate: (${latitude}, ${longitude})`);
                console.log(`    time: ${time}\n`);
            }
            console.log('----------------------------');
        }
    }

    runCompression() {
      console.log('\n\n--> Start Compression...');
      const fs = require('fs');

      const traj = this.removeDuplicatePoints(this.cleanPoints.map(p => p.coordinate));
      const { complexTraj, result } = compressTraj(traj, config.compression);

      const HEADER = 'latitude, longitude\n';
      const stringify = (arr) => (HEADER +
          arr.filter(p => typeof(p.lat) !== 'undefined' && typeof(p.lng) !== 'undefined')
          .map(p => (`${p.lat}, ${p.lng}`))
          .join('\n')
      );
      const stringify2 = (arr) => (HEADER +
          arr.filter(p => typeof(p.latitude) !== 'undefined' && typeof(p.longitude) !== 'undefined')
          .map(p => `${p.latitude}, ${p.longitude}`)
          .join('\n')
      );


      console.log(`Orig: ${this.cleanPoints.length} points`);
      console.log(`Before: ${complexTraj.length} points`);
      console.log(`After: ${result.length} points`);

      // const prefix = './logs/comp-logs'
      const prefix = './logs/valid/comp-logs'
      const handleErr = (err) => { if (err) console.error(err); }

      if (!fs.existsSync(prefix)) {
        fs.mkdirSync(prefix, 0o755, handleErr);
      }

      let data = stringify(complexTraj);
      fs.writeFile(`${prefix}/complex.csv`, data, handleErr);

      data = stringify(result);
      fs.writeFile(`${prefix}/comp.csv`, data, handleErr);

      data = stringify2(traj);
      fs.writeFile(`${prefix}/orig.csv`, data, handleErr);
  }
}

function simulate(err, data) {
  if (err) {
    console.error(err);
  }

  const trajs = sortData(data)
  const trips = splitToTrips(trajs, config.trip.len); // trajectory with specific time range
  numTrips = trips.length;

  console.log(`Num trips: ${numTrips}`);

  for (const trip of trips) {
    const pper = new Preprocesser(trip);
    pper.start();
  }

  fs.writeFile('./logs/stayTrajs.json'
    , JSON.stringify(gStayTrajs)
    , (err) => {
      if (err) {
        console.error(err)
      }
    });

  mongoose.disconnect();
}

function formatCoordinate(rawCoord) {
  if (typeof rawCoord === 'string') {
    const c = rawCoord.split(',').map(s => parseFloat(s));
    return {latitude: c[0], longitude: c[1]};
  } else {
    return rawCoord;
  }
}

function validate(data) {
  const trajs = data.map(traj => {
    if (traj.uid === 9994) {
      console.log('kiki: ', traj.points.length);
    }
    const tmp = traj.points.map(p => {
      const coordinate = formatCoordinate(p.coordinate);
      const time = new Date(p.time * 1000);

      return Object.assign({}, p, {coordinate, time})
    });
    const points = _.sortBy(tmp, p => p.time);
    return Object.assign({}, traj, {points});
  });

  const trips = splitToTrips(trajs, config.trip.len, config.trip.duration); // trajectory with specific time range
  numTrips = trips.length;
  console.log(`Num trips: ${numTrips}`);

  for (const trip of trips) {
    const pper = new Preprocesser(trip);
    pper.start();
  }

  fs.writeFile('./logs/stayTrajs.json'
    , JSON.stringify(gStayTrajs)
    , (err) => {
      if (err) { console.error(err); }
    }
  );

  mongoose.disconnect();
}

export default function() {
    const query = {};
    const fields = {};
    // getTrajs(query, fields).exec(simulate);
    validate(myTrajs);
}
