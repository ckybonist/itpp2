const path = require('path');
const Baby = require('babyparse');
const Promise = require('bluebird');
const fs = Promise.promisifyAll(require('fs'));
const _ = require('lodash');


function createTimestamp(unixTime) {
  const dt = new Date(unixTime * 1000);
  const date = `${dt.getFullYear()}-${dt.getMonth()+1}-${dt.getDate()}`;
  const time = `${dt.getHours()}:${dt.getMinutes()}:${dt.getSeconds()}`;
  return { obj: dt, string: `${date}__${time}` };
}

function parse(content) {
  const csvParser = Baby.parse;
  const data = csvParser(content).data
    .slice(1)
    .map((row) => {
      const unixTime = _.last(row);
      const timestamp = createTimestamp(unixTime);
      return {
        userId: row[526],
        phoneId: row[527],
        lat: row[521], lng: row[520],
        building: row[523], floor: row[522],
        roomId: row[524],
        timestamp
      };
    });

  return data;
}

function recordsOnDate(data, {year, month, day}) {
  return data.filter((row) => {
    const dt = row.timestamp.obj;
    return dt.getFullYear() === year
      && dt.getMonth()+1 === month
      && dt.getDate() === day;
  });
}

function show(data) {
  const date = {
    year: 2013,
    month: 6,
    day: 20,
  };

  const data2 = recordsOnDate(data, date)
    .filter(r => r.userId === '2');

  const traj2 = _.sortBy(data2, r => r.timestamp.obj);

  const floorsUser2 = _.uniq(data2.map(r => r.floor));
  const buildingsUser2 = _.uniq(data2.map(r => r.building));
  const locUser2 = _.uniqWith(data2.map(r => ({
    building: r.building,
    floor: r.floor
  })), _.isEqual);


  // console.log(`Buildings: ${buildingsUser2}`);
  // console.log(`Floors: ${floorsUser2}`);
  // console.log(`Locations:`);
  // locUser2.forEach(loc => console.log(`b${loc.building}, f${loc.floor}`));

  for (const row of traj2) {
    console.log(`User: ${row.userId}`);
    console.log(`Building: ${row.building}`);
    console.log(`Floor: ${row.floor}`);
    console.log(`Room: ${row.roomId}`);
    console.log(`Coordinate: ${row.lat}, ${row.lng}`);
    console.log(`Time: ${row.timestamp.string}`);
    console.log();
  }
}

function run() {
  const PATH = path.resolve(__dirname, '../../raw-data/trainingData.csv');

  fs.readFileAsync(PATH, 'utf8')
    .then(parse)
    .then(show)
    .catch(err => console.error(err));
}

run();
