//const key = 'AIzaSyALp7QfjRRZV_9t1qTnTx_GpUu1R0vFsTw';
//const key = 'AIzaSyA_MO4-xG7WU4cuusX32aP1Ec2spK1HJpM';
const key = 'AIzaSyB7m--U0D499X0bxDaQME1HL_SSTQsMAsw';

const googleMap = require('@google/maps').createClient({ key });

const Taipei101 = '25.034234, 121.564467';
const SysMemorialHall = '25.040010, 121.560216';
const YTStation = '25.040931, 121.576279';

const query = {
  mode: 'walking',
  origins: ['39.992507579488596, -0.0674775212002005'],
  destinations: ['39.9924460776161, -0.06752210818103556'],
};

const callback = (err, response) => {
  console.log(err, response);
  if (err) {
    console.log(err);
  } else {
    const result = response.json;
    for (const row of result.rows) {
      console.log(row.elements);
    }
  }
};


googleMap.distanceMatrix(query, callback);
