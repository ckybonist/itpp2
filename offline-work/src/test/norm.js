const math = require('mathjs');

const data = [
  [0.8, 0.5, 0.3, 0.7],
  [0.2, 0.13, 0.5, 0.8],
  [0.12, 0.82, 0.31, 0.44],
];

const M = math.matrix(data);
const NM = M.map(x => {
  return normalize(x, math.max(M), math.min(M));
});

console.log(NM);

function normalize(x, max, min) {
  return (x-min) / (max-min);
}
