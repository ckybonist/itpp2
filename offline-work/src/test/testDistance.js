function calculateDistance(src, dest) {
  Number.prototype.toRadians = function() {
    return this * Math.PI / 180;
  }

  const R = 6371e3;
  const rLat1 = src.lat.toRadians();
  const rLat2 = dest.lat.toRadians();

  const dLat = (dest.lat - src.lat).toRadians();
  const dLng = (dest.lng - src.lng).toRadians();

  const a = Math.sin(dLat/2) * Math.sin(dLat/2) +
            Math.cos(rLat1) * Math.cos(rLat2) *
            Math.sin(dLng/2) * Math.sin(dLng/2);
  const c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1-a));

  return R * c;
}

function test() {
  const src = {
    lat: 39.992507579488596, lng: -0.0674775212002005,
  };

  const dest = {
    lat: 39.9924460776161, lng: -0.06752210818103556,
  };

  const distance = calculateDistance(src, dest);
  console.log(distance);
}

test();
