import mongoose from 'mongoose';
import _ from 'lodash';
import getTrajectories from '../db';

main();

function main() {
    const query = {};
    const fields = {};
    getTrajectories(query, fields).exec(handler);
}

function handler(err, trajectories) {
    if (err)
        console.error(err);

    const sortByTime = (traj) => {
        const points =  _.sortBy(traj.points, p => p.time);
        return points;
    };

    const removeDuplicates = (points) => {
        const lastIdx = points.length - 1;
        const isEqual = (p1, p2) => (
            (p1.latitude === p2.latitude)
            && (p1.longitude === p2.longitude)
        );

        const result = points
            .map((p, idx) => {
                if (idx === lastIdx) return;

                const cur = p.coordinate;
                const next = points[idx + 1].coordinate;
                const duplicate = isEqual(cur, next);

                if (!duplicate) return p;
            })
            .filter(_.isObject);

        return result;
    };

    const trajs = trajectories
                    .map(sortByTime)
                    .map(removeDuplicates);

    for (const points of trajs) {
        showCoordinate(points);
        break;
    }

    mongoose.disconnect();
}

function showCoordinate(points) {
    for (const p of points) {
        const {latitude, longitude} = p.coordinate;
        console.log(`${latitude}, ${longitude} ... ${p.time.toGMTString()}`);
    }
}
