const _ = require('lodash');
const math = require('mathjs');


const interests = [
  [0.8, 0.5, 0.3, 0.7],
  [0.2, 0.13, 0.5, 0.8],
  [0.12, 0.82, 0.31, 0.44],
];

const numLocations = interests[0].length;
const numUsers = interests.length;
const hotness = Array(numLocations).fill([1/numLocations]);
const activeness = Array(numUsers).fill([1/numUsers]);
const ROUNDS = 100;
const EPSILON = 0.0000000000000001;

const M = math.matrix(interests);
let Hcur = math.matrix(hotness);
let Acur = math.matrix(activeness);
let Hprev = [];
let Aprev = [];
let cnvgH = -1;  // converge round of H
let cnvgA = -1;  // converge round of A

console.log(interests.map(r => (_.sum(r) / numLocations)));
console.log(math.transpose(M)._data.map(v => _.sum(v)));



console.log('# Hotness(H) and Activeness(A):');
for (let i = 0; i < ROUNDS; i++) {
  const errH = iterateH();
  const errA = iterateA();
  const round = i+1;

  console.log(`H at ${round}th rnd -->  `, _.flatten(Hcur._data));
  console.log(`A at ${round}th rnd -->  `, _.flatten(Acur._data));
  console.log();

  if (errH < EPSILON && cnvgH === -1) { cnvgH = round; }
  if (errA < EPSILON && cnvgA === -1) { cnvgA = round; }

  if (errA < EPSILON && errH < EPSILON) {
    console.log(`"H" Converge at ${cnvgH}th round: ${errH}`);
    console.log(`"A" Converge at ${cnvgA}th round: ${errA}`);
    break;
  }
}

const tmp = _.flatten(Hcur._data)
  .map((h, idx) => ({location: `room-${idx+1}`, intensity: h}));
const hotSpots = _.reverse(_.sortBy(tmp, ['intensity']));
console.log('\nHot Spots:');
hotSpots.forEach(hs => console.log(hs));



function iterateH() {
  Hprev = Hcur;
  Hcur = updateH(Hcur);
  const err = math.sum(
    math.subtract(Hcur, Hprev)
      .map(Math.abs)
  );

  return err;
}

function iterateA() {
  Aprev = Acur;
  Acur = updateA(Acur);
  const err = math.sum(
    math.subtract(Acur, Aprev)
      .map(Math.abs)
  );

  return err;
}

function updateH(Hprev) {
  const MT = math.transpose(M)
  const tmp = math.multiply(MT, M);
  return normalize(math.multiply(tmp, Hprev));
}

function updateA(Aprev) {
  const MT = math.transpose(M)
  const tmp = math.multiply(M, MT);
  return normalize(math.multiply(tmp, Aprev));
}

function normalize(m) {
  const max = math.max(m);
  const min = math.min(m);
  return m.map(e => (e - min) / (max - min));
}
