import calcSpeed from '../../utils/speed';
import filterNoise from '../../algorithms/NoiseFiltering';
import config from '../../config';


// User: 18, Building: 2, Floor: 1
const points = [
  {latitude: 39.9922000028143, longitude: -0.0665151820874818, time: 1371709794},
  {latitude: 39.99219511020102, longitude: -0.06651877085705284, time: 1371709832},
  {latitude: 39.99221830916376, longitude: -0.06655024037031847, time: 1371709847},
  {latitude: 39.99215241182915, longitude: -0.06652612536425624, time: 1371709881},

  /*
  {latitude: 39.99211629565195, longitude: -0.06655227801712105, time: 1371709907},
  {latitude: 39.992057043937585, longitude: -0.06646216621434806, time: 1371709938},
  */
  {latitude: 39.99211629565195, longitude: -0.06655227801712105, time: 1371709907},
  {latitude: 39.992057043937585, longitude: -0.06646216621434806, time: 1371709908},

  {latitude: 39.99204436193185, longitude: -0.06643233046881701, time: 1371709960},
  {latitude: 39.99200488094113, longitude: -0.0663394446684404, time: 1371709985},
  {latitude: 39.991958204478394, longitude: -0.06625208260874936, time: 1371710013},
  {latitude: 39.99193453485197, longitude: -0.06617394534871877, time: 1371710048},

  /*
  {latitude: 39.99192197051097, longitude: -0.06614438269103871, time: 1371710070},
  {latitude: 39.99188295469379, longitude: -0.06605259103866006, time: 1371710102},
  */
  {latitude: 39.99192197051097, longitude: -0.06614438269103871, time: 1371710070},
  {latitude: 39.99188295469379, longitude: -0.06605259103866006, time: 1371710071},

  {latitude: 39.99186972344715, longitude: -0.06602146261743914, time: 1371710131},

  /*
  {latitude: 39.991851183868214, longitude: -0.06593806122981735, time: 1371710166},
  {latitude: 39.99189146536065, longitude: -0.06590889203423266, time: 1371710191},
  */
  {latitude: 39.991851183868214, longitude: -0.06593806122981735, time: 1371710166},
  {latitude: 39.99189146536065, longitude: -0.06590889203423266, time: 1371710167},

  {latitude: 39.99192225956794, longitude: -0.06585965986507337, time: 1371710222},
  {latitude: 39.99191714876557, longitude: -0.06585995451248801, time: 1371710240},
  {latitude: 39.99190971577725, longitude: -0.06582386785678679, time: 1371710256},
  {latitude: 39.99238007881828, longitude: -0.06639209941884101, time: 1371710383},
].map((point) =>
  Object.assign({}, point, {
    time: new Date(point.time * 1000)
  })
);

const markers = [
  [4, 5], [10, 11], [13, 14],
];

const THRESHOLD = config.nf.THRES_SPEED;
console.log('=================== Start PP ==================');
console.log('User: 18, Building: 2, Floor: 1');
console.log(`${points.length} points in trajectory\n`);
console.log('Setup trajectory data...');
console.log('>>> 19 points after removing duplicates');
console.log('>>> reducing ratio: 5\n');
console.log('Thu, 20 Jun 2013 06:29:54 GMT');
console.log('Thu, 20 Jun 2013 06:39:43 GMT\n\n');

console.log('Start Noise Filtering...');
filterNoise(points, THRESHOLD);
console.log('=================== End PP ====================');

// markers.forEach((m) => {
//   const p1 = points[m[0]];
//   const p2 = points[m[1]];
//   console.log(calcSpeed(p1, p2));
// });
