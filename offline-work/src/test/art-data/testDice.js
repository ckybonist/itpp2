Number.prototype.precision = function() {
  const number = this.valueOf();
  if (!isFinite(number)) return 0;
  let e = 1;
  let result = 0;

  while(Math.round(number * e) / e !== number) {
    e = e * 10;
    result++;
  }

  return result;
};

function dice(min, max) {
  const delta = max - min;
  const number = Math.random() * delta + min;
  const prec = min.precision();
  return number.toFixed(prec);
}

(function test(i) {
  setTimeout(() => {
    const num = dice(-0.158, -0.132);
    if (num > -0.132 || num < -0.158)
      console.error('err: EXCEED RANGE');
    else
      console.log(num);
      
    if (--i) test(i);
  }, 800)
})(20);
