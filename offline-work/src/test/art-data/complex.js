Number.prototype.isPos = function() { return this.valueOf() > 0 };
Number.prototype.isNeg = function() { return this.valueOf() < 0 };
Number.prototype.precision = function() {
  const number = this.valueOf();
  if (!isFinite(number)) return 0;
  let e = 1;
  let result = 0;

  while(Math.round(number * e) / e !== number) {
    e = e * 10;
    result++;
  }

  return result;
};
Number.prototype.range = function() {
  const number = this.valueOf();
  return [...Array(number).keys()];
};

Array.prototype.solid = function() {
  const arr = this.valueOf();
  return arr.filter(e => e && typeof(e) !== 'undefined')
}

Array.prototype.merge = function() {
  const arr = this.valueOf();
  return arr.reduce((a, b) => a.concat(b));
}


const basePoints = [
  // {lat: 39.992817, lng: -0.067876},
  // {lat: 39.992930, lng: -0.067859},
  {lat: 39.992860, lng: -0.067674},
  {lat: 39.992751, lng: -0.067409},
  {lat: 39.992806, lng: -0.067307},
  // {lat: 39.992847, lng: -0.067221},
  // {lat: 39.992761, lng: -0.067201},
  // {lat: 39.992683, lng: -0.067196},
  // {lat: 39.992650, lng: -0.067206},
];
const NUM_RAND_POINTS = 20;

const result = basePoints
                .map(complicate)
                .merge()
                .solid();
console.log('latitude, longitude');
result.forEach(p => console.log(`${p.lat}, ${p.lng}`));



function complicate(cur, i, arr) {
  if (i === arr.length-1) return;

  const next = basePoints[i + 1];
  const coord = genRandCoord(cur, next);
  const iterator = NUM_RAND_POINTS.range();
  const intermediate = iterator.map(
    i => genRandCoord(cur, next)
  );

  return [].concat(cur, intermediate, next);
}

function genRandCoord(c1, c2) {
  return {
    lat: gen(c1.lat, c2.lat),
    lng: gen(c1.lng, c2.lng),
  };
}

function gen(a, b) {
  const delta = a - b;
  return (delta < 0) ? dice(a, b) : dice(b, a);
}

function dice(min, max) {
  const delta = max - min;
  const number = Math.random() * delta + min;
  const prec = min.precision();
  return number.toFixed(prec);
}
