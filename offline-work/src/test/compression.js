import compress from 'simplify-js';
import merc from 'mercator-projection';

import complicate from '../utils/data';


const points = [
  {latitude: 39.992817, longitude: -0.067876},
  {latitude: 39.992930, longitude: -0.067859},
  {latitude: 39.992860, longitude: -0.067674},
  {latitude: 39.992751, longitude: -0.067409},
  {latitude: 39.992806, longitude: -0.067307},
  {latitude: 39.992847, longitude: -0.067221},
  {latitude: 39.992761, longitude: -0.067201},
  {latitude: 39.992683, longitude: -0.067196},
  {latitude: 39.992650, longitude: -0.067206},
];

const NUM_RAND_POINTS = 150;
const THETA = 10000;
const TOLERANCE = 0.7;
const HIGH_QUALITY = false;

const transformScaling = (point) => ({
  x: point.x * THETA,
  y: point.y * THETA
});

const recoverScaling = (point) => ({
  x: point.x / THETA,
  y: point.y / THETA
});

const complexPoints = complicate(points, NUM_RAND_POINTS)
  .map(merc.fromLatLngToPoint)
  .map(transformScaling)
  .filter((p) => (!isNaN(p.x) && !isNaN(p.y)));

const result = compress(complexPoints, TOLERANCE, HIGH_QUALITY);

const show = (points) => {
  points
    .map(recoverScaling)
    .map(merc.fromPointToLatLng)
    .forEach((point) => console.log(`${point.lat}, ${point.lng}`));
};

console.log(`Before: ${complexPoints.length} points`);
show(complexPoints);
console.log('\n');

console.log(`After: ${result.length} points`);
show(result);
