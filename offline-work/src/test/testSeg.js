import spSeg from '../algorithms/Segmentation';



const trajectory = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12];


const splitPoints = [
	{ startIndex: 2, endIndex: 2 },
	{ startIndex: 6, endIndex: 6 },

	// { startIndex: 2, endIndex: 4 },  // 3 ~ 5
	// { startIndex: 7, endIndex: 9 },  // 8 ~ 10
];

const segments = spSeg(trajectory, splitPoints);

segments.forEach(s => {
	console.log(s);
});
