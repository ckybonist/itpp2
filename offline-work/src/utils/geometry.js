
export function meanCoordinate(region) {
  const NUM_POINTS = region.length;
  let sumLat = 0.0;
  let sumLng = 0.0;

  const myRegion = region.map(point => {
    //const coord = point.position.split(',');
    const c = point.coordinate;
    return {
      lat: parseFloat(c.latitude),
      lng: parseFloat(c.longitude)
    };
  });

  for (const point of myRegion) {
    sumLat += point.lat;
    sumLng += point.lng;
  }

  return {
    lat: sumLat / NUM_POINTS,
    lng: sumLng / NUM_POINTS
  };
}

export function calcDistance(src, dest) {
  Number.prototype.toRadians = function() {
    return this * Math.PI / 180;
  }

  const R = 6371e3;
  const rLat1 = src.latitude.toRadians();
  const rLat2 = dest.latitude.toRadians();

  const dLat = (dest.latitude - src.latitude).toRadians();
  const dLng = (dest.longitude - src.longitude).toRadians();


  const a = Math.sin(dLat/2) * Math.sin(dLat/2) +
            Math.cos(rLat1) * Math.cos(rLat2) *
            Math.sin(dLng/2) * Math.sin(dLng/2);
  const c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1-a));

  return R * c;
}
