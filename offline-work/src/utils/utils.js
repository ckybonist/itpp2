import fs from 'fs'
import path from 'path'
import _ from 'lodash';

import number from './number';


export function readJSONFile(filename, callback) {
  fs.readFile(filename, function (err, data) {
    if(err) {
      callback(err);
      return;
    }
    try {
      callback(null, JSON.parse(data));
    } catch (exception) {
      callback(exception);
    }
  });
}

export function parseTimeStr(ts) {
  // 20130620__07:36:49
  const year = ts.slice(0, 4);
  const month = ts.slice(4, 6);
  const day = ts.slice(6, 8);
  const hour = ts.slice(10, 12);
  const minute = ts.slice(13, 15);
  const second = ts.slice(16, ts.length);

  return new Date(year, month, day, hour, minute, second);
}

export function dateTimeStr(obj) {
  const year = obj.getFullYear();
  const month = obj.getMonth() + 1;
  const date = obj.getDate();
  const hours = obj.getHours();
  const minutes = obj.getMinutes();
  const seconds = obj.getSeconds();

  return `${year}-${month}-${date}  ${hours}:${minutes}:${seconds}`;
}

export function dirname(filename) {
    return path.join(__dirname + '/' + filename);
}

export function ms2sec(time) {
  return Math.abs(time / 1000);
}

/*
*  From https://gist.github.com/eranbetzalel/9f16b1216931e20775ad
*/
export function nGrams(array, length) {
  let ngramsArray = [];

  for (let i = 0; i < array.length - (length - 1); i++) {
      var subNgramsArray = [];

      for (let j = 0; j < length; j++) {
          subNgramsArray.push(array[i + j])
      }

      ngramsArray.push(subNgramsArray);
  }

  return ngramsArray;
}

/*
  From: http://stackoverflow.com/questions/9524354/reference-to-slice-of-an-array
*/
export function ArraySlice(arr, lo, hi) {
  this.arr = arr;
  this.lo = lo;
  this.hi = hi;
  this.length = hi - lo;
};

ArraySlice.prototype._contains = function(ix) {
  return this.lo + ix < this.hi;
};

ArraySlice.prototype.get = function(ix) {
  if (this._contains(ix)) {
      return this.arr[this.lo + ix];
  }
};

export function range(upper) {
  return [...Array(upper).keys()];
}

function dice(min, max) {
    const delta = max - min;
    const number = Math.random() * delta + min;
    return number;
}

/*
 * Iterate array with current and next element
 *
 * @arr: Array
 * @fn: (current, next) => { ... }
 */
function pairwise(arr, fn) {
  for (let i = 0; i < arr.length-1; i++) {
    const cur = arr[i];
    const next = arr[i+1];
    fn(cur, next);
  }
}

export function notNaN(num) {
  return !isNaN(num);
}

export function descSort(arr, key) {
  return _.reverse(_.sortBy(arr, key));
}
