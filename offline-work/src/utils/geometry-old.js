import gmapClient from '@google/maps'
import Promise from 'bluebird'

import { onClientSide } from './utils'
import config from './config'


export function computeMeanCoordinate(region) {
  const NUM_POINTS = region.length;
  let sumLat = 0.0;
  let sumLng = 0.0;

  const myRegion = region.map(point => {
    //const coord = point.position.split(',');
    const c = point.coordinate;
    return {
      lat: parseFloat(c.latitude),
      lng: parseFloat(c.longitude)
    };
  });

  for (const point of myRegion) {
    sumLat += point.lat;
    sumLng += point.lng;
  }

  return {
    lat: sumLat / NUM_POINTS,
    lng: sumLng / NUM_POINTS
  };
}

export function calculateDistance(orig, dest, travelMode) {
  const queryInterval = 800;  // ms
  const query = initDistanceMatrixReq(
      [ orig ],
      [ dest ],
      travelMode
  );

  return new Promise((resolve, reject) => {
    distanceMatrix(query, queryInterval)
      .then(result => {
        const distRows = result.rows;
        const distance = distRows[0].elements[0].distance.value;
        resolve(distance === 0 ? 1 : distance);
      })
      .catch(err => {
        console.error(`Error at calculateDistance(): ${err}`);
      });
  });
}

export function distanceMatrix(query, interval) {
  const nodeGoogleMap = gmapClient.createClient({
    key: config.gmapKey,
    Promise: Promise
  });

  const service = nodeGoogleMap.distanceMatrix;

  const run = () => {
    return nodeGoogleMap.distanceMatrix(query).asPromise()
      .then(response => {
        const result = response.json;
        const status = result.status;

        if (status !== 'OK') {
          // reject({ err, status })
          console.log('\n\nUnnormal status: ', status);
          return status;
        } else {
          // resolve(result);
          return result;
        }
      })
      .catch(err => {  // socket hang up
        console.error(err);
        // console.log('Resend distance matrix request...');
      });
  };

  return Promise.delay(interval).then(run);
}

export function distanceMatrixForClient(query, interval) {
  const service = new google.maps.DistanceMatrixService
  let _query = Object.assign({}, query);

  return new Promise((resolve, reject) => {
    const forClient = () => {
      // Modify request's format for primitive Directions API
      _query.mode = googleTraveMode(_query.mode);
      service.getDistanceMatrix(_query, (response, status) => {
         if (status === maps.DirectionsStatus.OK) {
           resolve(response);
         } else {
           reject(status);
         }
      });
    };

    Promise.delay(interval)
      .then(forClient);
  });
}

export function initDistanceMatrixReq(origins, destinations, travelMode) {
  const _origins = origins.map(googleLatLng);
  const _destinations = destinations.map(googleLatLng);

  return {
      origins: _origins,
      destinations: _destinations,
      mode: travelMode,
  };
}

function googleLatLng(coordinate) {
  const c = coordinate;
  return `${c.latitude}, ${c.longitude}`;
}

function googleTraveMode(mode) {
  const travelMode = google.maps.TravelMode;
  switch (mode.toLowerCase()) {
    case 'walking':
      return travelMode.WALKING;
    case 'driving':
      return travelMode.DRIVING;
    case 'bicycling':
      return travelMode.BICYCLING;
    case 'transit':
      return travelMode.TRANSIT;
    default:
      console.error('Unexpected travel mode!')
      return null;
  }
}
