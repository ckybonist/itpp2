import { ms2sec } from './utils'
import { calcDistance } from './geometry';


export default function calcSpeed(src, dest) {
  const p1 = format(src);
  const p2 = format(dest);

  const distance = calcDistance(p1, p2);
  const duration = ms2sec(p2.time - p1.time);
  const speed = distance / (duration === 0 ? 1 : duration);

  return speed === 0 ? 1 : speed;
}

function format(point) {
  const keys = Object.keys(point);
  if (keys.includes('latitude') &&
      keys.includes('longitude') &&
      keys.includes('time')) {
      return point;
  }

  const { latitude, longitude } = point.coordinate;
  const time = point.time;

  return {
    latitude,
    longitude,
    time,
  }
}

/*
export function collectSpeeds(points) {
  const speeds = [];
  const numPts = points.length;

  points.forEach((p1, i) => {
    if (i >= numPts-1) return;

    const p2 = points[i+1];
    speeds.push(calcSpeed(p1, p2, 'walking'));
  });

  return Promise.all(speeds);
}

export function calcSpeed(p1, p2, travelMode) {
  return calculateDistance(p1.coordinate, p2.coordinate, travelMode)
    .then(distance => {
      // Durations
      const duration = ms2sec(p2.time - p1.time);

      // Speeds
      return distance / duration;
    })
}
*/
