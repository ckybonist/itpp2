export default () => {};

Array.prototype.solid = function() {
  const arr = this.valueOf();
  return arr.filter(e => e && typeof(e) !== 'undefined')
}

Array.prototype.merge = function() {
  const arr = this.valueOf();
  return arr.reduce((a, b) => a.concat(b));
}
