import _ from 'lodash';
import number from './number';
import array from './array';


export default function complicate(points, numPoints) {
  function generate(cur, i, arr) {
    if (i === arr.length-1) return;

    const next = arr[i + 1];
    const coord = genRandCoord(cur, next);
    const intermediate = _.range(numPoints).map(
      (i) => genRandCoord(cur, next)
    );

    return [].concat(cur, intermediate, next);
  }

  return points.map(generate)
          .merge()
          .solid();
}


function genRandCoord(c1, c2) {
  return {
    lat: gen(c1.latitude, c2.latitude),
    lng: gen(c1.longitude, c2.longitude),
  };
}

function gen(a, b) {
  const delta = a - b;
  return (delta < 0) ? dice(a, b) : dice(b, a);
}

function dice(min, max) {
  const delta = max - min;
  const number = Math.random() * delta + min;
  const prec = min.precision();
  return number.toFixed(prec);
  return number;
}
