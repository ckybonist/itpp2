const path = require('path');
const fs = require('fs');


export default function readStayTrajs() {
  const fpath = path.resolve(__dirname, '../logs/stayTrajs.json');
  const data = fs.readFileSync(fpath);
  const trajs = JSON.parse(data);
  return refineDataFormat(trajs);
  // return trajs;
}

function refineDataFormat(trajs) {
  return trajs.map(t => {
      const { user, building, floor } = t.id;
      const data = t.data.map(p => {
          const location = {
            room: p.location.id,
            // inside: p.location.inside,  // filter out `inside` property
            building,
            floor
          };
          return Object.assign({}, p, {location});
        });
      return {user, data};
    });
}
