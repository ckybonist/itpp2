const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const Model = mongoose.model.bind(mongoose);


const MetaData= Schema({
    trajIDs: [{ uid: Number, building: Number, floor: Number }],
    userIDs: [Number],
    numBuildings: Number,
    numFloors: [Number],
    numPhones: Number,
});

const Point = Schema({
  coordinate: {
    latitude: Number,
    longitude: Number
  },
  location: {
    id: String,
    inside: Boolean
  },
  time: Date,
  rssis: [Number]
});

const Trajectory= Schema({
  building: Number,
  floor: Number,
  uid: Number,
  pid: Number,
  points: [Point],
});

module.exports = {
  MetaData: Model('MetaData', MetaData),
  Trajectory: Model('Trajectory', Trajectory),
};

//export default mongoose.model('Trajectory', TrajectorySchema);
