# 室內軌跡資料處理程式

##
此專案為室內軌跡資料處理機制的實驗程式，這是我的碩士研究。軌跡資料處理機制包含 7 項重要
的程序：從最初的 Wi-Fi 感測器記錄處理一直到最終的特徵擷取。

此專案程式並未包含感測器記錄處理的實作，因為研究使用的資料已經將感測紀錄處理完成。

**關於室內軌跡資料處理機制的詳細內容，請參考 [[論文]](https://drive.google.com/open?id=1NUfyl-gjlPVd-bf8GW7E2-2CzC9Us_1i)**

## 程式
主要程式模組放置於`/offline-work/src/algorithms`，包括：
- 異常位置點過濾: NoiseFiltering.js

- 特殊點偵測：spd.js

- 軌跡分割：SPSeg.js

- 軌跡壓縮：Compression.js

- 特徵擷取：
	- 熱門地點、熱門路徑：hotSpot.js, hotRoute.js
	- 再訪次數：revisit.js


## 資料庫
1. Install mongodb. e.g. `brew install mongodb`
2. `mkdir $your-path/db`, and set this path to mongod.conf's dbpath field.
3. `mkdir $your-path/db/run`
4. Create pid file for background running: `touch $your-path/db/run/mongo.pid`
5. `mongod --config mongod.conf`
