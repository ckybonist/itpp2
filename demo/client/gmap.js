plotTraj();

function plotTraj() {
  $.getJSON("/traj", function(data) {
    let trajectory = data.map(point => {
      return {
        lat: parseFloat(point.latitude),
        lng: parseFloat(point.longitude),
        time: parseTimeStr(point.timestamp)
      };
    });

    const center_point = gmapCoord(trajectory[0].lat, trajectory[0].lng);
    const googleMap = initGoogleMap(center_point);
    //setMarkers(googleMap, trajectory);
    drawPath(googleMap, trajectory);
  });
}

function initGoogleMap(center_point) {
  return new google.maps.Map(document.getElementById('map'),
    {
      zoom: 19,
      center: center_point
    });
}

function setMarkers(googleMap, traj) {
  for (const point of traj) {
    const image = {
      url: '/images/map_pin.png',
      size: new google.maps.Size(30, 30),
      origin: new google.maps.Point(0, 0),
      anchor: new google.maps.Point(17, 34),
      scaledSize: new google.maps.Size(25, 25)
    };

    const marker = new google.maps.Marker({
      position: gmapCoord(point.lat, point.lng),
      map: googleMap,
      icon: image,
    });
  }
}

function drawPath(googleMap, traj) {
  const lineSymbol = {
    path: google.maps.SymbolPath.FORWARD_CLOSED_ARROW
    // path: 'M 0,-1 0,1',
    // strokeOpacity: 1,
    // scale: 3
  };

  const poly = new google.maps.Polyline({
      strokeColor: '#000000',
      strokeOpacity: 1.0,
      strokeWeight: 2,
      icons: [{
        icon: lineSymbol,
        offset: '100%',
        //repeat: '20px'
      }],
      map: googleMap,
  });

  const path = poly.getPath();

  for (let i = 0; i < traj.length; i++) {
    const point = traj[i];
    const coord = gmapLatLng(point.lat, point.lng);
    path.push(coord);
    if (i === 0 || i === traj.length-1) {  // set big marker for orig and dest point
      setMarker(googleMap, coord);
    }
    else {
      const icon = newMarkerIcon('images/map_pin.png');
      setMarker(googleMap, coord, icon);
    }

  }


  // for (const point of traj) {
  //   const coord = gmapLatLng(point.lat, point.lng);
  //   path.push(coord);
  //   const marker = new google.maps.Marker({
  //     position: coord,
  //     title: '#' + path.getLength(),
  //     icon: image,
  //     map: googleMap
  //   });
  // }
}

function setMarker(googleMap, coord, icon) {
  let options = {
      position: coord,
      map: googleMap
  };

  if (icon) {
    options.icon = icon;
  }

  const maker = new google.maps.Marker(options);
}

function newMarkerIcon(imgUrl) {
  return {
    url: imgUrl,
    size: new google.maps.Size(20, 32),
    origin: new google.maps.Point(0, 0),
    anchor: new google.maps.Point(0, 32),
    scaledSize: new google.maps.Size(30, 30)
  };
}



class StayPoint {
  constructor(meanCoord, arrivalTime, leaveTime) {
    this.meanCoord = meanCoord;
    this.arrivalTime = arrivalTime;
    this.leaveTime = leaveTime;
  }
}

function detectStayPoints(traj, thres_dist, thres_time) {
  let result = [];
  return new Promise(async(resolve, reject) => {
    const NUM_POINTS = traj.length;
    console.log(`Number of Points: ${NUM_POINTS}`);
    const [THRES_DIST, THRES_TIME] = [thres_dist, thres_time];
    let i = 0;
    let j = 0;

    while (i < NUM_POINTS) {
      /* IMPORTANT:
       * Prevent infinite loop that happend when:
       *  j === NUM_POINTS - 1
       */
      if (j > NUM_POINTS-1) { break; }

      j = i + 1;
      while (j < NUM_POINTS) {

        const pi = traj[i];
        const pj = traj[j];
        try {
          const dist_info = await calcDistance(pi, pj, i, j);
          const element = dist_info.rows[0].elements[0];
          const distance = element.distance.value; // meter
          const duration = element.duration.value; // minute

          if (distance >= THRES_DIST) {
            console.log('Found region');
            const timeDelta = (pj.time - pi.time) / 1000;

            if (timeDelta < 0) {
              throw new Error('Order of timestamp is not correct...');
            } else if (timeDelta > THRES_TIME) {
              console.log('Found stay point');
              const region = traj.slice(i, j + 1); // points of region
              const stayPoint = new StayPoint(computeMeanCoord(region), pi.time, pj.time);
              result.push(stayPoint);
            }

            i = j;
            break;
          }
        } catch (err) {
          throw err;
        }
        j = j + 1;
      }
    } // END WHILE
    resolve(result);
  });
}

async function calcDistance(orig, dest, i, j) {
  const origins = [gmapLatLng(orig.lat, orig.lng)];
  const destinations = [gmapLatLng(dest.lat, dest.lng)];

  return new Promise((resolve, reject) => {
    const service = new google.maps.DistanceMatrixService();
    const options = {
      origins: origins,
      destinations: destinations,
      travelMode: google.maps.TravelMode.WALKING
    };

    const getDist = () => {
      service.getDistanceMatrix(options, (response, status) => {
        const distStatus = google.maps.DistanceMatrixStatus;

        switch(status) {
          case distStatus.OK:
            resolve(response);
            break;
          case distStatus.OVER_QUERY_LIMIT:
            console.log(`Point that OVER QUERY LIMIT: i: ${i}  ---  j: ${j}`);
          default:
            reject(status);
        }
      });
    };

    setTimeout(getDist, 100);
  });
}

function gmapCoord(lat, lng) {
  return {
    lat: lat,
    lng: lng
  };
}


function computeMeanCoord(points) {
  const NUM_POINTS = points.length;
  let sumLat = 0.0;
  let sumLng = 0.0;

  for (const p of points) {
    sumLat += p.lat;
    sumLng += p.lng;
  }

  return {
    lat: sumLat / NUM_POINTS,
    lng: sumLng / NUM_POINTS
  };
}

function parseTimeStr(ts) {
  // 20130620__07:36:49
  const year = ts.slice(0, 4);
  const month = ts.slice(4, 6);
  const day = ts.slice(6, 8);
  const hour = ts.slice(10, 12);
  const minute = ts.slice(13, 15);
  const second = ts.slice(16, ts.length);

  return new Date(year, month, day, hour, minute, second);
}

  const gmapLatLng = (lat, lng) => {
    return new google.maps.LatLng(lat, lng);
  };
