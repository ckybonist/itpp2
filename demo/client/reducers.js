/**
 * Root Reducer
 */
import { combineReducers } from 'redux';

// Import Reducers
//import app from './modules/App/AppReducer';
import trajectory from './modules/Showcases/TrajReducer';

// Combine all reducers into one root reducer
export default combineReducers({
  traj: trajectory,
});
