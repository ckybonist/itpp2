import React from 'react'
import { Route, IndexRoute } from 'react-router';
import Base from './modules/Base/Base'


// require.ensure polyfill for node
if (typeof require.ensure !== 'function') {
  require.ensure = function requireModule(deps, callback) {
    callback(require);
  };
}

/* Workaround for async react routes to work with react-hot-reloader till
  https://github.com/reactjs/react-router/issues/2182 and
  https://github.com/gaearon/react-hot-loader/issues/288 is fixed.
 */
if (process.env.NODE_ENV !== 'production') {
  // Require async routes only in development for react-hot-reloader to work.
  require('./modules/Overview/pages/OverviewPage');
  require('./modules/Showcases/pages/ShowcasesPage');
}


/*
* NOTE: Only root route and index route will fetch data
* on the initial time at server-side.
*
*/

export default (
  <Route path="/" component={Base}>
    <IndexRoute
      getComponent = {(nextState, cb) => {
        require.ensure([], (require) => {
          cb(null, require('./modules/Overview/pages/OverviewPage').default)
        })
      }}
    />
    <Route
      path="/showcases"
      getComponent = {(nextState, cb) => {
        require.ensure([], (require) => {
          cb(null, require('./modules/Showcases/pages/ShowcasesPage').default)
        })
      }}
    />
    <Route
      path="/showcases/testd3"
      getComponent = {(nextState, cb) => {
        require.ensure([], (require) => {
          cb(null, require('./modules/Showcases/pages/D3Page').default)
        })
      }}
    />
  </Route>
);

// <IndexRoute component={Overview} />
// <Route path="/showcases" component={TrajVisualizer} />
