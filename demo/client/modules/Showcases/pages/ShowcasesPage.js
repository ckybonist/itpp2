import React, { Component } from 'react';
import { connect } from 'react-redux';
import CircularProgress from 'material-ui/CircularProgress'
import RaisedButton from 'material-ui/RaisedButton'

import { fetchMetaInfo, fetchTraj } from '../TrajActions'
import { getMetaInfo, getTraj } from '../TrajReducer'
import TrajVisualizer from '../components/Toolbox/TrajVisualizer'


export class ShowcasesPage extends Component {
  componentDidMount() {
    this.props.dispatch(fetchTraj(12, 2, 1));
    this.props.dispatch(fetchMetaInfo());
  }

  render() {
    if (!this.props.metaInfo || this.props.traj.length === 0) {  // Insure data is loaded
      return (
        <div style={styles.progress}>
          <CircularProgress size={200} thickness={15} />
        </div>
      );
    }

    return (
      <div>
        <TrajVisualizer
          metaInfo={this.props.metaInfo}
          trajectory={this.props.traj}
        />
      </div>
    );
  }
}


// Actions required to provide data for this component to render in sever side.
ShowcasesPage.need = [
  () => {
    return fetchMetaInfo();
  },
  () => {
    return fetchTraj(12, 2, 1);  // default traj to show
  },
];

// Retrieve data from store as props
function mapStateToProps(state, props) {
  return {
    metaInfo: getMetaInfo(state),
    traj: getTraj(state),
  };
}

export default connect(mapStateToProps)(ShowcasesPage);


const styles = {
  progress: {
    position: 'absolute',
    top: '40%',
    left: '40%',
  },
};
