import React, { Component } from 'react'
import { ScatterChart, PieChart } from 'react-d3'
import _ from 'lodash'

import { b2f1u7 as speeds } from './speedLog'


function scatterChartData(data) {
  const to2d = (v, x) => {
    const y = (!v) ? 1 : v;
    return { x, y };
  };

  const values = data.map(to2d);//.filter(obj => obj.y > 0.5);
  const truths = Array(data.length).fill(1.2).map(to2d);

  return [
    { name: 'noisy', values },
    // { name: 'Kalman filter', values: {} },
    { name: 'truth', values: truths }
  ];
}

function pieChartData(data) {
  const total = data.length;

  const calc = (part) => {
    const dominant = data.filter(e => e === part).length;
    return parseFloat((dominant / total * 100).toFixed(3));
  }

  const parts = _.uniqWith(data).map(pt => {
    return (!pt) ? 1 : pt;
  });

  const smallParts = parts.map(part => calc(part))
    .filter(percent => percent < 1)
    .reduce((a, b) => a + b);

  let pieData = parts.map(part => (
      {
        label: part.toFixed(2).toString(),
        value: calc(part),
      }
    ))
    .filter(e => e.value >= 1);

  pieData = Object.entries(_.groupBy(pieData, (e) => e.value))
    .map(e => (
      {
        label: e[1][0].label,
        value: e[0]
      }
    ));


  pieData.push({ label: 'Outliers', value: smallParts.toFixed(3) });

  return pieData;
}


export default class D3Page extends Component {
  render() {
    const scatterData = scatterChartData(speeds);
    const pieData = pieChartData(speeds);

    return (
      <div>
        <ScatterChart
          data={scatterData}
          width={500}
          height={400}
          title="Speeds of Points"
        />
        <PieChart
          data={pieData}
          width={800}
          height={600}
          radius={200}
          innerRadius={30}
          sectorBorderColor="white"
          title="Pie Chart"
        />
      </div>
    );
  }
}
