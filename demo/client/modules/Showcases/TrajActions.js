import callApi from '../../utils/apiCaller';


// Labels
export const FETCH_METAINFO = 'FETCH_METAINFO';
export const FETCH_TRAJ = 'FETCH_TRAJ'
export const FETCH_STAY_POINTS = 'FETCH_STAY_POINTS';

// Actions
export function fetchMetaInfoAction(metaInfo) {
  return {
    type: FETCH_METAINFO,
    metaInfo
  }
}

export function fetchTrajAction(traj) {
  return {
    type: FETCH_TRAJ,
    traj
  };
}

export function fetchStayPointsAction(stayPoints) {
  return {
    type: FETCH_STAY_POINTS,
    stayPoints
  }
}

export function fetchMetaInfo() {
  return (dispatch) => {
    return callApi('/trajs/meta')
      .then(res => dispatch(fetchMetaInfoAction(res.metaInfo)));
  };
}

export function fetchTraj(user, building, floor) {
  return (dispatch) => {
    return callApi(`trajs/${user}?building=${building}&floor=${floor}`)
          .then(res => dispatch(fetchTrajAction(res.traj)));
  };
}

// TODO: use post method
export function fetchStayPoints() {
  return (dispatch) => {
    return callApi('/traj/stay-points')
      .then(res => dispatch(fetchStayPointsAction(res.stayPoints)));
  };
}
