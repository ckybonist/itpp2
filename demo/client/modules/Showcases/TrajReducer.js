import { FETCH_METAINFO, FETCH_TRAJ } from './TrajActions'

const initialState = {
  metaInfo: null,
  data: []
};

const TrajReducer = (state=initialState, action) => {
  switch (action.type) {
    case FETCH_METAINFO:
      return Object.assign({}, state, {
        metaInfo: action.metaInfo
      });

    case FETCH_TRAJ:
      return Object.assign({}, state, {
        data: action.traj
      });

    default:
      return state;
  }
};


/* selectors */

// Trajectory data's meta info which contains
// 1. User IDs
// 2. Building IDs
// 3. Floor IDs
export const getMetaInfo = (state) => state.traj.metaInfo;

// Get trajectory by user-id, building-id and floor-id
export const getTraj = (state) => state.traj.data;


export default TrajReducer;
