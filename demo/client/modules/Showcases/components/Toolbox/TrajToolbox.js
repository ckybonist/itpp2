import React, { Component } from 'react'
import { connect } from 'react-redux';
import {Tabs, Tab} from 'material-ui/Tabs';
import Paper from 'material-ui/Paper'
import CircularProgress from 'material-ui/CircularProgress'

import { fetchTraj } from '../../TrajActions'
import TrajSelector from './TrajSelector'
import AlgorithmSelector from './AlgorithmSelector'


/*
  TODO: The error occurs when switching from page Overview to TrajVisualizer
  at the first time. May be caused by ajax data not loaded.
*/
export class TrajToolbox extends Component {
  constructor(props) {
    super(props);
    this.state = {
      user: 12,
      building: 2,
      floor: 1,
    };
    this.handleTrajSelect = this.handleTrajSelect.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
    this.fetchTargetTraj = this.fetchTargetTraj.bind(this);
  }

  fetchTargetTraj() {
    const { user, building, floor } = this.state;
    this.props.dispatch(fetchTraj(user, building, floor));
  }

  handleTrajSelect(newState) {
    this.setState(newState);
  }

  handleSubmit() {
    this.fetchTargetTraj();
  }

  render() {
    return (
      <Paper style={style} zDepth={2}>
        <Tabs>
          <Tab label="選擇器">
            <TrajSelector
              onSelect={this.handleTrajSelect}
              onSubmit={this.handleSubmit}
              trajIndex={this.state}
              metaInfo={this.props.metaInfo}
            />
          </Tab>
          <Tab label="演算法">
            <AlgorithmSelector />
          </Tab>
        </Tabs>
      </Paper>
    );
  }
}

// Retrieve data from store as props
function mapStateToProps(state, props) {
  return {
  };
}

export default connect(mapStateToProps)(TrajToolbox);


const style = {
  width: 295,
  height: '100%',
  display: 'inline-block',
  margin: '16px 32px 16px 0',
};
