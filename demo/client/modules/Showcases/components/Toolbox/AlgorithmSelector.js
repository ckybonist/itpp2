import React, { Component } from 'react'
import RaisedButton from 'material-ui/RaisedButton'

export default class AlgorithmSelector extends Component {
  handleClick = () => {
    console.log('detecting stay points...');
  }

  render() {
    return (
      <div>
        <RaisedButton label="detect stay points" secondary={true} onClick={this.handleClick} />
      </div>
    );
  }
}
