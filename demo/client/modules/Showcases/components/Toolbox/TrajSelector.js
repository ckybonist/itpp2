import React, { Component } from 'react'
import SelectField from 'material-ui/SelectField'
import MenuItem from 'material-ui/MenuItem'
import RaisedButton from 'material-ui/RaisedButton'
import Divider from 'material-ui/Divider'
import CircularProgress from 'material-ui/CircularProgress';


String.prototype.capitalizeFirst = () => {
  return this.charAt(0).toUpperCase() + this.slice(1);
}

export default class TrajSelector extends Component {

  render() {
    const { users, total_buildings, total_floors } = this.props.metaInfo;
    const tags = Object.keys(this.props.trajIndex);

    const MenuItemList = (value, tag) => (
      <MenuItem key={value} value={value} primaryText={`${tag} ${value}`} />
    );

    // TODO: Need more dynamic way...
    const menuItemIndices = (tag) => {
      switch (tag) {
        case 'user':
          return users;
        case 'building':
          return [...Array(total_buildings).keys()];
        case 'floor':
          return [...Array(total_floors).keys()];
        default:
          console.error('Unrecognized tag of meta info!');
      }
    };

    const mySelectFields = tags.map(tag => {
      return (
        <SelectField
          floatingLabelText={tag}
          value={this.props.trajIndex[tag]}
          key={tag}
          onChange={
            (event, index, value) => {
              let newState = {};
              newState[tag] = value;
              this.props.onSelect(newState);
            }
          }
        >
          {
            menuItemIndices(tag).map(id => {
              return MenuItemList(id, tag);
            })
          }
        </SelectField>
      );
    });

    return (
      <div>
        <div style={{margin: '1em'}}>
          <h4>Pick a trajectory</h4>
          { mySelectFields }
        </div>
        <br />
        <RaisedButton
          label="繪製軌跡"
          labelColor="#FFFFFF"
          backgroundColor="#00BCD4"
          fullWidth={true}
          onClick={this.props.onSubmit}
        />
      </div>
    );
  }
};
