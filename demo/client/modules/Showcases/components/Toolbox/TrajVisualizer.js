import React, { Component } from 'react'
import Paper from 'material-ui/Paper'

import SimpleMap from '../MyMap/SimpleMap'
import DirectionsMap from '../MyMap/DirectionsMap'
import TrajToolbox from './TrajToolbox'


export default class TrajVisualizer extends Component {
  getPosition = (step) => {
    const lat = step.coordinate.latitude;
    const lng = step.coordinate.longitude;
    // return { position: {lat, lng} };
     return { lat, lng };
  }

  // Due to only 8 waypoints can be passed to google map
  // (https://developers.google.com/maps/documentation/javascript/3.exp/reference?hl=zh-tw#DirectionsStatus)
  // , so that we have to select candidate points in terms of
  // time order.
  prepareWayPoints = (candidates) => {
    const maxPoints = 8;
    const interval = Math.round(candidates.length / maxPoints);
    const format = (e) => ({ location: e, stopover: true });

    return candidates
            .filter((e, idx) => (idx % interval === 0))
            .slice(0, maxPoints)
            .map(format);
  }

  prepareData = (data) => {
    const lastIdx = data.length - 1;
    const myData = data.map(this.getPosition);
    const origin = myData[0];
    const destination = myData[lastIdx];
    const waypoints = this.prepareWayPoints(myData.slice(1, lastIdx));
    return { origin, waypoints, destination };
  }

  render() {
    const trajectory = this.prepareData(this.props.trajectory);

    return (
      <div>
        <Paper zDepth={2}>
          {/* <SimpleMap markers={ this.props.markers } /> */}
          <DirectionsMap trajectory={trajectory} />
        </Paper>

        <div style={styles.toolbox}>
          <TrajToolbox metaInfo={this.props.metaInfo} />
        </div>
      </div>
    );
  }
}



const styles = {
  simpleMap: {
    float: 'left',
    textAlign: 'center',
    display: 'inline-block',
  },
  toolbox: {
    marginLeft: '50%',
    padding: '5em',
    overflow: 'hidden',
  }
}
