import {
  default as React,
  Component,
} from "react";

import {
  withGoogleMap,
  GoogleMap,
  DirectionsRenderer,
  Marker,
} from 'react-google-maps'

import withScriptjs from 'react-google-maps/lib/async/withScriptjs'
import CircularProgress from 'material-ui/CircularProgress'

import gmapConfig from './config'



class MyGoogleMap extends Component {
  constructor(props) {
    super(props);

    this.state = { directions: null, };
  }

  componentDidUpdate() {
    const DirectionsService = new google.maps.DirectionsService();
    const handler = (result, status) => {
      if (status === google.maps.DirectionsStatus.OK) {
        this.setState({
          directions: result,
        });
      }
      else {
        console.error(`error fetching directions ${result}`);
      }
    };

    const { origin, waypoints, destination } = this.props.trajectory;
    const query = {
      origin,
      destination,
      waypoints,
      optimizeWaypoints: false,
      travelMode: google.maps.TravelMode.WALKING,
    };

    DirectionsService.route(query, handler);
  }


  render() {
    const { origin, destination } = this.props.trajectory;
    return (
      <GoogleMap
        defaultZoom={18}
        defaultCenter={this.props.center}
      >
        <Marker
          position={origin}
          label="START"
        />
        <Marker
          position={destination}
          label="END"
        />
        {
          this.state.directions &&
          <DirectionsRenderer
            directions={this.state.directions}
            options={{
              suppressMarkers: true,
              hideRouteList: true,
            }}
            routeIndex={1}
          />
        }
      </GoogleMap>
    );
  }
}


const DirectionsGoogleMap =
    withScriptjs(withGoogleMap(MyGoogleMap));


export default class DirectionsMap extends Component {
  render() {
    const trajectory = this.props.trajectory;

    return (
      <DirectionsGoogleMap
        googleMapURL={`https://maps.googleapis.com/maps/api/js?v=3.exp&key=${gmapConfig.key}`}
        loadingElement={
          <div style={styles.map}>
            <CircularProgress size={100} />
          </div>
        }
        containerElement={
          <div style={styles.map} />
        }
        mapElement={
          <div style={styles.map} />
        }
        center={trajectory.origin}
        trajectory={trajectory}
      />
    );
  }
}

const styles = {
  map: {
    float: 'left',
    width: '600px',
    height: '700px',
    magin: 0,
    overflow: 'hidden',
  },
};
