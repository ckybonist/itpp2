import React from 'react'
import { withGoogleMap, GoogleMap, Marker, Polyline } from 'react-google-maps'
import withScriptjs from 'react-google-maps/lib/async/withScriptjs'
import CircularProgress from 'material-ui/CircularProgress';

import gmapConfig from './config'
import mapStyle from './MapStyle'
import markerImg from './assets/map-pin.svg'



const GettingStartedGoogleMap = withScriptjs(
  withGoogleMap(props => {

    console.log(`total points: ${props.markers.length}`);


    const myIcon = {
      url: markerImg,
      size: new google.maps.Size(30, 30),
    };

    const polylineOptions = {
      strokeColor: '#212121',
      strokeOpacity: 1,
      strokeWeight: 1,
    }

    return (
      <GoogleMap
        ref={props.onMapLoad}
        defaultZoom={19}
        defaultCenter={props.center.position}
        defaultOptions={{ styles: mapStyle }}
        onClick={props.onMapClick}
      >
        {
          props.markers.map((marker, index) => (
            <Marker
              key={index}
              {...marker}
              onRightClick={() => props.onMarkerRightClick(index)}
              // icon={myIcon}
            />
          ))
        }
        <Polyline
          path={ props.markers.map(p => p.position ) }
          options={polylineOptions}
        />
      </GoogleMap>
    );
  })
);

export default class SimpleMap extends React.Component {
  handleRightClick(index) {
    console.log(index);
  }

  render() {
    const markers = this.props.markers;

    return (
      <GettingStartedGoogleMap
        googleMapURL={`https://maps.googleapis.com/maps/api/js?v=3.exp&key=${gmapConfig.key}`}
        loadingElement={
          <div style={styles.simpleMap}>
            <CircularProgress size={80} />
          </div>
        }
        containerElement={
          <div style={styles.simpleMap} />
        }
        mapElement={
          <div style={styles.simpleMap} />
        }
        // onMapLoad={_.noop}
        // onMapClick={_.noop}
        markers={markers}
        center={markers[5]}
        onMarkerRightClick={this.handleRightClick}
     />
    );
  }
};

const styles = {
  simpleMap: {
    float: 'left',
    width: '600px',
    height: '700px',
    magin: 0,
    overflow: 'hidden',
  },
};
