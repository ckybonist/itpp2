import React from 'react'
import ReactDom from 'react-dom'
import pointImg from './map_pin.png'


function newPoint(coord) {
  const pointStyle = `
    width: 10px;
    height: '10px';
    left: ${coord.x}px;
    top: ${coord.y}px;
    position: absolute;
    cursor: pointer;
  `;

  const imgStyle = `
    position: absolute;
    width: 15px;
    height: 15px;
    top: -6px;
    left: -6px;
  `;

  const img = document.createElement('img');
  img.setAttribute('clsss', 'map-pin')
  img.setAttribute('src', pointImg);
  img.setAttribute('style', imgStyle);

  const point = document.createElement('div');
  point.setAttribute('class', 'map-point');
  point.setAttribute('style', pointStyle);
  point.appendChild(img);

  return point;
}

function MyOverlay(map) {
  this.layerID = 'mylayer';
  this.setMap(map);
}

function onAdd() {
  console.log('on layer add');

  if (document.getElementById(this.layerID)) {
    onRemove.apply(this);
  }

  this.myLayer = document.createElement('div');
  this.myLayer.id = this.layerID;

  const pane = this.getPanes().overlayImage;
  pane.appendChild(this.myLayer);
}

function onRemove() {
  console.log('on layer remove');
  const myLayer = document.getElementById(this.layerID);
  myLayer.parentNode.removeChild(myLayer);
  this.myLayer = null;
}

function createRandomMarkers(mapsAPI) {
  const southWest = new mapsAPI.LatLng(40.744656,-74.005966);  // Los Angeles, CA
	const northEast = new mapsAPI.LatLng(34.052234,-118.243685); // New York, NY
	const lngSpan = northEast.lng() - southWest.lng();
	const latSpan = northEast.lat() - southWest.lat();

  let points = [];
  for (let i = 0; i < 1000; i++) {
    // Determine a random location from the bounds set previously
		const coordinate = new mapsAPI.LatLng(
				southWest.lat() + latSpan * Math.random(),
				southWest.lng() + lngSpan * Math.random());
    points.push(coordinate);
  }

  return points
}

function clearLayer() {
  console.log('clear layer');
  const myLayer = this.myLayer;
  while (myLayer.hasChildNodes()) {
    myLayer.removeChild(myLayer.firstChild);
  }
}

function draw() {
  console.log('On layer draw');

  clearLayer.apply(this);

  const projection = this.getProjection();
  const zoom = this.getMap().getZoom();
  const points = document.createDocumentFragment();

  //createRandomMarkers(google.maps).map(coord => {
  g_markers.map(coord => {
    const pixelCoord = projection.fromLatLngToDivPixel(coord);
    const point = newPoint(pixelCoord);
    points.appendChild(point);
  });

  this.myLayer.appendChild(points);
}

let g_markers = [];  // global variable

//function initOverlay(mapsAPI, map, markers) {
function initOverlayView() {
  if ( !(MyOverlay.prototype instanceof google.maps.OverlayView) )
  {
    MyOverlay.prototype = new google.maps.OverlayView();
    MyOverlay.prototype.onAdd = onAdd;
    MyOverlay.prototype.onRemove = onRemove;
    MyOverlay.prototype.draw = draw;
  }
}

function createMapOverlay(map, markers) {
  g_markers = markers.map(p => {
    return new google.maps.LatLng(p.lat, p.lng);
  });

  initOverlayView();

  if (overlayMap)
    overlayMap = null;

  let overlayMap = new MyOverlay(map);
}

export default createMapOverlay;
