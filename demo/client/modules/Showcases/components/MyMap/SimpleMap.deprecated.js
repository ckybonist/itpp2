import {
  default as React,
  Component,
  PropTypes,
} from "react";

import loadGoogleMapsApi from 'load-google-maps-api'
import createMapOverlay from './MapOverlay'
import APIOpts from './config.js'
import styles from './map.css'


export default class SimpleMap extends Component {
  createMap(center) {
    this.map = new google.maps.Map(this.refs.map, {
        center: center,
        zoom: 6
    });

    createMapOverlay(
      this.map,
      this.props.trajectory
    );
  }

  loadMap() {
    loadGoogleMapsApi(APIOpts)
      .then((mapsAPI) => {
        this.createMap(this.props.trajectory[0]);
      })
      .catch((err) => {
        console.error(err);
      });
  }

  componentDidMount() {
    if (!window.google) {
      this.loadMap();
    } else {
      this.createMap(this.props.trajectory[0]);
    }
  }

  componentWillReceiveProps(nextProps) {
    const trajectory = nextProps.trajectory;
    this.map.center = trajectory[0];
    createMapOverlay(this.map, trajectory);
  }

  render() {
    return (
      <div className={styles.simpleMap} ref="map">
      </div>
    );
  }
}
