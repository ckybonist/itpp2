import React, { Component } from 'react'
import { Link } from 'react-router'
import FlatButton from 'material-ui/FlatButton';

export default class NavButtons extends Component {
  constructor(props) {
    super(props);
    this.state = { selectedIndex: 0 };
    this.select = (index) => this.setState({selectedIndex: index});
  }

  render() {
    return (
      <div>
        <Link to={'/'}>
          <FlatButton label="Overview" secondary={true} onTouchTap={() => this.select(0)}>
          </FlatButton>
        </Link>

        <Link to={'/showcases'}>
          <FlatButton label="Showcases" secondary={true} onTouchTap={() => this.select(1)}>
          </FlatButton>
        </Link>

        <Link to={'/showcases/testd3'}>
          <FlatButton label="Test D3" secondary={true} onTouchTap={() => this.select(2)}>
          </FlatButton>
        </Link>

        <Link to={'/about'}>
          <FlatButton label="About" primary={true} onTouchTap={() => this.select(3)}>
          </FlatButton>
        </Link>
      </div>
    );
  };
}
