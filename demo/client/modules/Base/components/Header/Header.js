import React, { Component } from 'react'
import AppBar from 'material-ui/AppBar'
import MenuIcon from 'material-ui/svg-icons/navigation/menu'
import IconButton from 'material-ui/IconButton'
import NavButtons from './NavButtons'


class LeftIconElement extends Component {
  render () {
    const leftElement = styles.appBar.leftElement;

    return (
      <IconButton
        iconStyle={leftElement.iconStyle}
        style={leftElement.style}
      >
        <MenuIcon color="#FF1744"/>
      </IconButton>
    );
  }
}

export default class Header extends Component {
  render() {

    return (
      <div style={styles.header}>
        <AppBar
          style={styles.appBar.style}
          titleStyle={styles.appBar.titleStyle}
          title="軌跡資料前處理"
          zDepth={2}
          height={60}
          iconElementLeft={ <LeftIconElement /> }
          iconElementRight={ <NavButtons /> }
        />
      </div>
    );
  };
}

const styles = {
  header: {
    position: 'fixed',
    width: '100%',
    left: 0,
    top: 0,
    zIndex: 100,
    borderTop: 0,
  },
  appBar: {
    style: {
      backgroundColor: '#FFFFFF',
    },
    titleStyle: {
      color: '#424242',
      fontSize: 22,
    },
    leftElement: {
      iconStyle: { width: 36, height: 36 },
      style: { width: 80, height: 50, padding: 0, }
    },
  },
}
