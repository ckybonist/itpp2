import React from 'react'
import { connect } from 'react-redux'
import DevTools from './components/DevTools'
import Helmet from 'react-helmet'

import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider'
import getMuiTheme from 'material-ui/styles/getMuiTheme'
import { white, grey800, grey600, greenA400 } from 'material-ui/styles/colors'

import Header from './components/Header/Header'


export class Base extends React.Component {
  // componentWillUnmount() {
  //   window.removeEventListener('scroll', this.handleScroll, false);
  // }
  //
  // componentDidMount() {
  //   this.setState({isMounted: true}); // eslint-disable-line
  //   window.addEventListener('scroll', this.handleScroll, false);
  // }
  //
  // handleScroll(event) {
  //   const topOffset = event.target.body.scrollTop;
  //   const boundary = 40;
  //
  //   if (topOffset > boundary) {
  //     const newTheme = Object.assign({}, defaultMuiTheme, {
  //       appBar: {height: 45}
  //     });
  //     this.setState({muiTheme: newTheme , zDepth: 2});
  //
  //   } else if (topOffset <= boundary) {
  //     this.setState(this.myDefaultState);
  //   }
  // }


  render() {
    const muiTheme = getMuiTheme({
      fontFamily: 'Noto Sans TC, sans-serif',
      palette: {
        textColor: '#757575',
      },
    });

    return (
      <div>
        <Helmet
          title="My App"
          titleTemplate="%s - Visualizer"
          meta={[
            { charset: 'utf-8' },
            {
              'http-equiv': 'X-UA-Compatible',
              content: 'IE=edge',
            },
            {
              name: 'viewport',
              content: 'width=device-width, initial-scale=1',
            },
          ]}
        />
        <MuiThemeProvider muiTheme={muiTheme}>
          <div>
            <Header />
            <div style={styles.container}>
              { this.props.children }
            </div>
          </div>
        </MuiThemeProvider>
      </div>
    );
  }
}

// Retrieve data from store as props
function mapStateToProps(store) {
  return {

  };
}

export default connect(mapStateToProps)(Base);  // DON'T REMOVE THIS LINE


const styles = {
  container: {
    maxWidth: '80vw',
    width: '100%',
    paddingTop: '100px',
    margin: '0 auto',
  },
}
