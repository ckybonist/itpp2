import React from 'react';
import { render } from 'react-dom';
import { AppContainer as HotContainer } from 'react-hot-loader';
import { configureStore } from './store'
import injectTapEventPlugin from 'react-tap-event-plugin'

import App from './App';

// Initialize store
const store = configureStore(window.__INITIAL_STATE__);
const mountPoint = document.getElementById('root');

// Enable Material-UI's tap event
injectTapEventPlugin()

render (
  <HotContainer>
    <App store={store}/>
  </HotContainer>
  , root
)

if (module.hot) {
  module.hot.accept('./App', () => {
    const NextApp = require('./App').default;

    render(
        <HotContainer>
          <NextApp store={store} />
        </HotContainer>
      , mountPoint)
  })
}
