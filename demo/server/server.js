import Express from 'express';
import compression from 'compression'
import bodyParser from 'body-parser'
import mongoose from 'mongoose'

// Webpack Requirements
import webpack from 'webpack'
import config from '../webpack.config.dev'
import webpackDevMiddleware from 'webpack-dev-middleware'
import webpackHotMiddleware from 'webpack-hot-middleware'

// Init Express
const app = new Express();

// Run webpack dev server in development mode
if (process.env.NODE_ENV === 'development') {
  const compiler = webpack(config);
  app.use(webpackDevMiddleware(compiler, {
    noInfo: true,
    publicPath: config.output.publicPath,
    // hot: true,
    // headers: { "Access-Control-Allow-Origin": "*" }
  }));
  app.use(webpackHotMiddleware(compiler));
}


// Common
import path from 'path'

// Routes
import routes from '../client/routes'
import api from './routes/api'

// React
import { configureStore } from '../client/store';
import { Provider } from 'react-redux';
import React from 'react'
import { renderToString } from 'react-dom/server'
import { match, RouterContext } from 'react-router'
import Helmet from 'react-helmet'
import injectTapEventPlugin from 'react-tap-event-plugin'


// Mine
import { fetchComponentData } from './utils/fetchData';
import serverConfig from './config'



// Set native Promise as mongoose's Promise which is deprecated
mongoose.Promise = global.Promise;

// DB connection
mongoose.connect(serverConfig.mongoURL, err => {
  if (err) {
    console.error('Please make sure Mongodb is installed and running!'); // eslint-disable-line no-console
    throw err;
  }
});


// Apply body Parser and server public assets and routes
app.use(compression());
app.use(bodyParser.json({ limit: '20mb' }));
app.use(bodyParser.urlencoded({ limit: '20mb', extended: false }));
app.use(Express.static(path.resolve(__dirname, '../dist')));

if (module.hot) {
  module.hot.accept(['./routes/api', '../client/routes'], () => {
    console.log('🔁  HMR Reloading `./app`...');
  });
  console.info('✅  Server-side HMR Enabled!');
} else {
  console.info('❌  Server-side HMR Not Supported.');
}

app.use('/api', (req, res) => api.handle(req, res));


// Material-UI for server-rendering
global.navigator = { userAgent: 'all' };
// Enable Material-UI's tap event
injectTapEventPlugin();

// Render Initial HTML
const renderFullPage = (html, initialState) => {
  const head = Helmet.rewind();

  // Import Manifests
  const assetsManifest = process.env.webpackAssets && JSON.parse(process.env.webpackAssets);
  const chunkManifest = process.env.webpackChunkAssets && JSON.parse(process.env.webpackChunkAssets);

  return `
    <!doctype html>
    <html>
      <head>
        ${head.base.toString()}
        ${head.title.toString()}
        ${head.meta.toString()}
        ${head.link.toString()}
        ${head.script.toString()}
        ${process.env.NODE_ENV === 'production' ? `<link rel='stylesheet' href='${assetsManifest['/app.css']}' />` : ''}
        <link href="https://fonts.googleapis.com/css?family=Roboto:300,400,500" rel="stylesheet">
      </head>
      <style type="text/css">
        @import url(http://fonts.googleapis.com/earlyaccess/notosanstc.css);
        body {
          font-family: 'Noto Sans TC, sans-serif';
          background-color: #FAFAFA;
        }
      </style>
      <body>
        <div id="root">${process.env.NODE_ENV === 'production' ? html : `<div>${html}</div>`}</div>
        <script>
          window.__INITIAL_STATE__ = ${JSON.stringify(initialState)};
          ${process.env.NODE_ENV === 'production' ?
          `//<![CDATA[
          window.webpackManifest = ${JSON.stringify(chunkManifest)};
          //]]>` : ''}
        </script>
        <script src='${process.env.NODE_ENV === 'production' ? assetsManifest['/vendor.js'] : '/vendor.js'}'></script>
        <script src='${process.env.NODE_ENV === 'production' ? assetsManifest['/app.js'] : '/app.js'}'></script>
      </body>
    </html>
  `;
};

const renderError = err => {
  const softTab = '&#32;&#32;&#32;&#32;';
  const errTrace = process.env.NODE_ENV !== 'production' ?
    `:<br><br><pre style="color:red">${softTab}${err.stack.replace(/\n/g, `<br>${softTab}`)}</pre>` : '';
  return renderFullPage(`Server Error${errTrace}`, {});
};


// Match routes by React-Router
app.use((req, res, next) => {
  match({routes, location: req.url}, (err, redirectLocation, renderProps) => {
    if (err) {
      return res.status(500).end(renderError(err));
    }

    if (redirectLocation) {
      return res.redirect(302, redirectLocation.pathname + redirectLocation.search);
    }

    if (!renderProps) {
      return next();
    }

    const store = configureStore();

    return fetchComponentData(store, renderProps.components, renderProps.params)
      .then(() => {
        const initialView = renderToString(
          <Provider store={store}>
            <RouterContext {...renderProps} />
          </Provider>
        );

        const finalState = store.getState();

        res.set('Content-Type', 'text/html')
          .status(200)
          .end(renderFullPage(initialView, finalState));
      })
      .catch((error) => next(error));
  });
});


app.listen(serverConfig.port, (err) => {
  if (!err) {
    console.log(`My app is running on port ${serverConfig.port}!`);
  }
});

export default app;
