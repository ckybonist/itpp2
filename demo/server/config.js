const config = {
  mongoURL: process.env.MONGO_URL || 'mongodb://localhost:27017/Analysis',
  port: process.env.PORT || 9000,
};

export default config;
