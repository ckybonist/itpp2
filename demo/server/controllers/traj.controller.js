import Trajectories from '../../common/models/trajectory'


export function getMetaInfo(req, res) {
  console.log('Receive meta-info req');
  const metaInfo = {
    users: [7, 12, 16],
    total_buildings: 3,
    total_floors: 5,
  };
  res.json({metaInfo});
}

export function getTraj(req, res) {
  console.log('Receive traj req');
  const user = parseInt(req.params.user);
  const building = parseInt(req.query.building);
  const floor = parseInt(req.query.floor);
  const query = {
    uid: user,
    'coordinate.building': building,
    'coordinate.floor': floor,
  };

  Trajectories.find(query).sort({ time: 1 }).exec()  // sort time by ascending order
    .then(traj => {
      res.json({ traj });
    })
    .catch(err => {
      res.status(500).send(err);
    });
}
