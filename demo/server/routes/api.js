import { Router } from 'express';

const router = new Router();

router.use(require('./traj.routes').default);

router.route('/test/hot').get((req, res) => {
  res.json({date: `✅  Date: yo<strong>${new Date()}</strong>`});
});

export default router;
