import { Router } from 'express'
import * as TrajController from '../controllers/traj.controller'
const router = new Router()

// Get metaInfo
router.route('/trajs/meta').get(TrajController.getMetaInfo)

// Get trajectory by building, phone and user ID.
router.route('/trajs/:user').get(TrajController.getTraj)


export default router
