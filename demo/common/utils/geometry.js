import Promise from 'bluebird'
import { onClientSide } from './utils'


export function gmapCoord(lat, lng) {
  return {
    lat: lat,
    lng: lng
  };
}

export function computeMeanCoord(region) {
  const NUM_POINTS = region.length;
  let sumLat = 0.0;
  let sumLng = 0.0;

  const myRegion = region.map(point => {
    const coord = point.position.split(',');
    return {
      lat: parseFloat(coord[0]),
      lng: parseFloat(coord[1])
    };
  });

  for (const point of myRegion) {
    sumLat += point.lat;
    sumLng += point.lng;
  }

  return {
    lat: sumLat / NUM_POINTS,
    lng: sumLng / NUM_POINTS
  };
}


export function googleDirectionDistance(query, nodeGoogleMap, interval) {
  return new Promise((resolve, reject) => {

    const directionDistanceForClient = () => {
      // Modify request's format for primitive Directions API
      query.origin = query.origin[0];
      query.destination = query.destination[0];
      //query.travelMode = getPrimitiveGoogleMapsTraveMode(query.travelMode);
      query.travelMode = query.travelMode.toUpperCase();

      const maps = google.maps;
      const directionsService = new maps.DirectionsService;

      directionsService.route(query, (response, status) => {
         if (status === maps.DirectionsStatus.OK) {
           resolve(response);
         } else {
           console.log(status);
         }
      });
    };

    const directionDistanceForServer = () => {
      // Modify request's format for Node.js Directions API
      query.mode = query.travelMode;
      delete query.travelMode;

      nodeGoogleMap.directions(query, (err, response) => {
        const result = response.json;
        const status = result.status;
        if (err) {
          reject(err);
        } else if (status === 'NOT_FOUND') {
          reject(status)
        } else {
          const route = result.routes[0].legs[0];
          resolve(route.distance);
        }
      });
    }

    Promise.delay(interval)
      .then(onClientSide()
        ? directionDistanceForClient
        : directionDistanceForServer);
  });
}

function getPrimitiveGoogleMapsTraveMode(mode) {
  const travelMode = google.maps.TravelMode;
  switch (mode.toLowerCase()) {
    case 'walking':
      return travelMode.WALKING;
    case 'driving':
      return travelMode.DRIVING;
    case 'bicycling':
      return travelMode.BICYCLING;
    case 'transit':
      return travelMode.TRANSIT;
    default:
      console.error('Unexpected travel mode!')
      return null;
  }
}
