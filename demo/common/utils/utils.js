import fs from 'fs'
import path from 'path'

export function onClientSide() {
  return (typeof(window) !== 'undefined' && window !== null);
}

export function readJSONFile(filename, callback) {
  fs.readFile(filename, function (err, data) {
    if(err) {
      callback(err);
      return;
    }
    try {
      callback(null, JSON.parse(data));
    } catch (exception) {
      callback(exception);
    }
  });
}

export function parseTimeStr(ts) {
  // 20130620__07:36:49
  const year = ts.slice(0, 4);
  const month = ts.slice(4, 6);
  const day = ts.slice(6, 8);
  const hour = ts.slice(10, 12);
  const minute = ts.slice(13, 15);
  const second = ts.slice(16, ts.length);

  return new Date(year, month, day, hour, minute, second);
}

export function dirname(filename) {
    return path.join(__dirname + '/' + filename);
}
