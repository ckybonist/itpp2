import mongoose from 'mongoose'
const Schema = mongoose.Schema

const TrajectorySchema = Schema({
  uid: Number,
  pid: Number,
  building: Number,
  floor: Number,
  coordinate: {
    latitude: Number,
    longitude: Number
  },
  location: {
    id: Number,
    inside: Boolean
  },
  time: Date,
  rssis: [Number]
});

export default mongoose.model('Trajectory', TrajectorySchema);
