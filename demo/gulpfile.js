const gulp    = require('gulp');
const webpack = require('gulp-webpack');
const server  = require('tiny-lr')();  // livereload server
const refresh = require('gulp-livereload');
const plumber = require('gulp-plumber');
const nodemon = require('gulp-nodemon');
const notify  = require('gulp-notify');

const webpackConf = require('./webpack.config.js');
const paths = {
  client: './public/js/plot-orig.js',
  html: './views/*.html',
  styles: './public/css/*.css',
  assets: './public/images/*',
  server: './server/*.js'
};
const lrPort = 9000;   // port for tiny-lr

gulp.task('default', ['build', 'lr', 'serve', 'watch']);

gulp.task('build', ['css', 'client']);

gulp.task('lr', () => {
  server.listen(lrPort, err => {
    if (err)
      return console.error(err);
  });
});

gulp.task('watch', () => {
  gulp.watch(paths.html, ['html']);
  gulp.watch(paths.client, ['client']);
  gulp.watch(paths.styles, ['css']);
});

gulp.task('serve', () => {
  nodemon({'script': './index.js'});
});

gulp.task('client', () => {
  return gulp.src(paths.client)
    .pipe(plumber())
    .pipe(webpack(webpackConf))
    .pipe(gulp.dest('./public/js/'))
    .pipe(refresh(server))
    .pipe(notify({
      message: 'Client js refreshed.'
    }));
});

gulp.task('css', () => {
  return gulp.src(paths.styles)
    .pipe(plumber())
    .pipe(refresh(server))
    .pipe(notify({
      message: 'Style refreshed'
    }));
});

gulp.task('html', () => {
  return gulp.src(paths.html)
    .pipe(plumber())
    .pipe(refresh(server))
    .pipe(notify({
      message: 'Views refreshed'
    }));
});
